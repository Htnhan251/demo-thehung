require('./bootstrap')

import Alpine from 'alpinejs'

window.Alpine = Alpine

Alpine.start()

$(document).ready(function () {
  $('.flexslider').flexslider({
    controlNav: false,
  })
})
