<?php 
return [
    'category' => 'Danh mục',
    'category_name' => 'Tên danh mục:',
    'title' => 'Tiêu đề:',
    'short_description' => 'Mô tả ngắn:',
    'description' => 'Mô tả:',
    'create_category' => 'Tạo danh mục',
    'create_new_category' =>'Tạo Danh mục mới',
    'edit_category' => 'Sửa Danh mục',
    'name' => 'Tên',
    'slug' => 'Slug',
    'active' => 'Trạng thái',
    'actions' => 'Hành động',
    'placeholder_name' => 'Tên danh mục',
    'placeholder_title' => 'Tiêu đề cho SEO',
];