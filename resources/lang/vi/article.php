<?php 
return [
    'article' => 'Bài viết',
    'article_name' => 'Tên bài viết:',
    'title' => 'Tiêu đề:',
    'is_active' => 'Trạng thái:',
    'short_description' => 'Mô tả ngắn:',
    'image' => 'Hình ảnh:',
    'image_size' => 'Kích thước hình ảnh: px',
    'description' => 'Mô tả:',
    'click_here_browse'=>'( Bấm vào để tải ... )',
    'create_article' => 'Tạo Bài viết',
    'create_new_article' => 'Tạo Bài viết mới', 
    'edit_article' => 'Sửa bài viết',
    'article' => 'Bài viết',
    'image' => 'Hình ảnh',
    'more' => 'Hành động',
    'placehover_name_article' => 'Tên bài viết. Vd: Iphone',
    'placehover_title' => 'Tiêu đề bài viết. Vd: Iphone '
];