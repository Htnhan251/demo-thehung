<?php 
return [
    'category' => 'Category',
    'category_name' => 'Category name:',
    'title' => 'Title:',
    'short_description' => 'Short description:',
    'description' => 'Description:',
    'create_category' => 'Create Category',   
    'create_new_category' =>'Create new Category',
    'edit_category' => 'Edit Category',
    'name' => 'Name',
    'slug' => 'Slug',
    'active' => 'Active',
    'actions' => 'Actions',
    'placeholder_name' => 'name of category',
    'placeholder_title' => 'title for SEO',
];