<div class="container">
    <div class="row">
        <div class="article col-md-12 bg-white p-5">
            <div class="detail">{!! $article->translated->long_description !!}</div>
            <iframe
                src="https://www.facebook.com/plugins/like.php?href={{ Request::url() }}&width=138&layout=button&action=like&size=large&share=true&height=65&appId"
                width="138" height="65" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                allowfullscreen="true"
                allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">
            </iframe>
            <div class="text-center">
                <a class="navbar-brand" href="/.">
                    <img src="{{ asset('images/logo.jpg') }}" width="80" height="80" class="d-inline-block align-top" />
                </a>
            </div>
        </div>
    </div>
</div>
