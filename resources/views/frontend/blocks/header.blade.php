<nav class="navbar navbar-expand-lg px-5 py-0">
    <div class="container-fluid">
        <a class="navbar-brand" href="/.">
            <img src="{{ asset(getLogo()) }}" width="78" height="78" class="d-inline-block align-top" />
        </a>
        <button class="navbar-toggler p-1" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 mx-auto">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                </li>
                @foreach ($categories as $category)
                    <li class="nav-item">
                        <a class="nav-link" href="/cate/{{ $category->translated->slug }}">
                            {{ $category->translated->name }}
                        </a>
                    </li>
                @endforeach

                <li class="nav-item">
                    <a class="nav-link" href="/about">
                        Giới thiệu
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/contact">
                        Liên hệ
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/article">
                        Tin Tức
                    </a>
                </li>
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        Dropdown
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li> --}}
                {{-- <li class="nav-item">
                    <a class="nav-link disabled">Disabled</a>
                </li> --}}



            </ul>
            {{-- <form method="POST" action="{{ route('search') }}" class="d-flex" role="search">
                @csrf
                <input name='keyword' class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form> --}}
            @livewire('front.search')
            <ul class="navbar-nav mb-2 mb-lg-0">
                @if (Auth::user())
                    {{-- Đức custom css infouser --}}
                    <!-- Authentication -->
                    <li class="rd-nav-item dropdown">
                        <a class="rd-nav-link " href="#" role="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <img src="https://img.lovepik.com/element/40144/0477.png_300.png" alt="avatar">
                        </a>
                        <ul class="dropdown-menu dropdown-user" style="left:-200px">
                            <div class="arrow-up-menu"></div>
                            <li class="li-avatar p-4 ">
                                <div class="row">
                                    <div class="col-12 col-sm-4 col-md-4 mb-2">
                                        <img src="https://img.lovepik.com/element/40144/0477.png_300.png"
                                            alt="avatar">
                                    </div>
                                    <div class="col-12 col-sm-8 col-md-8">
                                        <h5><b>Hello, {{ Auth::user()->name }}</b></h5>
                                        <p> {{ Auth::user()->is_admin ? 'Admin' : 'Custommer' }}</p>
                                        <a href="#" class="btn btn-sm btn-warning">Sửa</a>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a class="dropdown-item px-4 pt-3" href="{{ route('profile.index') }}"><i
                                        class="fa fa-user" aria-hidden="true"></i>
                                    Trang cá nhân
                                </a>
                            </li>
                            <li>
                                @if (Auth::user()->is_admin)
                                    <a class="dropdown-item px-4" href="/admin"><i class="fas fa-gamepad"></i> Controll
                                        panel</a>
                                @endif
                            </li>

                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li>
                                <form method="POST" action="{{ route('logout') }}" x-data>
                                    @csrf
                                    <a class='dropdown-item px-4' href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); this.closest('form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> {{ __('Log Out') }}
                                    </a>
                                </form>
                            </li>
                            <div x-arrow="" class="popper__arrow" style="left: 224px;"></div>
                        </ul>
                    </li>
                @endif
            </ul>





        </div>
    </div>
</nav>
