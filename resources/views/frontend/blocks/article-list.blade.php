@extends('frontend.master')
@section('content')
    <div class="page-heading text-center">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-content">
                    <h1 class="page-title">TIN TỨC</h1>
                    <div class="page-breadcrumb">
                        <span>
                            <a href="/"> <i class="fas fa-home"></i> Home</a>
                            <i class="fas fa-arrow-right"></i>
                            <a href="/article">
                                Tin tức
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="block-title col-md-12 text-center py-4">
                    <h1>
                        <span>ARTICLES</span>
                    </h1>
                    <p>Newest 2022</p>
                </div>
                @php
                    $n = 0;
                @endphp
                @foreach ($articaleList as $article)
                    @php
                        $date = \Carbon\Carbon::parse($article->translated->updated_at);
                        $n++;
                    @endphp

                    {{-- Đức Css bootstrap article --}}
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <div class="row ">
                            <div class="container-article">
                                <div class="background-article">
                                    <div class="image-article">
                                        <a class="link" href="/article/{{ $article->translated->slug }}">
                                            <img src="{{ $article->translated->image }}" alt=""
                                                style="width: 100%;">
                                        </a>
                                    </div>
                                    <div class="content-article">
                                        <div class="date text-center">{{ $date->day }} / {{ $date->format('M') }}</div>
                                        <div class="name text-center">
                                            <a class="link" href="/article/{{ $article->translated->slug }}">
                                                <h3>{{ $article->translated->name }}</h3>
                                            </a>
                                        </div>
                                        <div class="summary text-center">
                                            {{ myTextLimit($article->translated->short_description, 17) }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                @endforeach
                <div class="pages">{{ $articaleList->links() }}</div>

            </div>
        </div>
    </div>
@endsection
