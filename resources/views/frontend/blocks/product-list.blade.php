<div class="container">
    <div class="row">
        @foreach ($products as $product)
            <div class="product-list col-xs-12 col-sm-6 col-md-4 text-center">
                <div class="product-inner">
                    <div class="name">
                        <a href="/detail/{{ $product->translated->slug }}">
                            <h2>{{ $product->translated->name }}</h2>
                        </a>
                    </div>
                    <div class="category">
                        <a href="/cate/{{ $product->categories->translated->slug }}">
                            <h4>{{ $product->categories->translated->name }}</h4>
                        </a>
                    </div>
                    <div class="image">
                        <a href="/detail/{{ $product->translated->slug }}">
                            <img class="img-fluid" src="{{ $product->translated->image }}" />
                        </a>
                    </div>
                    <div class="price">
                        {{ number_format($product->price, 0, ',', '.') == 0 ? 'Liên hệ' : number_format($product->price, 0, ',', '.') . ' ' . 'VND' }}
                    </div>
                </div>
            </div>
        @endforeach
        <div class="pages">{{ $products->links() }}</div>
    </div>
</div>
