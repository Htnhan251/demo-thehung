<div class="container-fluid">
    <div class="row">
        <div class="image col-md-5">
            <img src="{{ $product->translated->image }}" class="img-fluid">
        </div>
        <div class="product-info col-md-7">
            <div class="name">
                <h1>{{ $product->translated->name }}</h1>
            </div>
            <div class="price">
                <h2>{{ number_format($product->price, 0, ',', '.') == 0 ? 'Liên hệ' : number_format($product->price, 0, ',', '.') . ' ' . 'VND' }}
                </h2>
            </div>
            <div class="short_description">{{ $product->translated->short_description }}</div>
            <div class="share">

                <iframe
                    src="https://www.facebook.com/plugins/like.php?href={{ Request::url() }}&width=138&layout=button&action=like&size=large&share=true&height=65&appId"
                    width="138" height="65" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                    allowfullscreen="true"
                    allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share">
                </iframe>
            </div>
        </div>

    </div>
</div>
