<div class="container">
    <div class="row">
        <div class="item col-md-6 col-sm-12">
            <h4 class="title">{{ $info->title_1 }}</h4>
            <div class="desc text-justify">
                {!! html_entity_decode($info->colum_1) !!}
            </div>
        </div>
        <div class="item col-md-3 col-sm-6">
            <h4 class="title">{{ $info->title_2 }}</h4>
            <div class="desc">
                {!! $info->colum_2 !!}
            </div>
        </div>
        <div class="item col-md-3 col-sm-6">
            <h4 class="title">{{ $info->title_3 }}</h4>
            <div class="desc">                
                {!! $info->colum_3 !!}
            </div>
        </div>
        <div class="copyright col-sx-12 text-center">{{$info->copyright}}</div>
    </div>
</div>