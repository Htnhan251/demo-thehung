<!DOCTYPE html>
<html lang="en">

<head>
  
  @include('frontend.partials.head')
  @livewireStyles
</head>

<body>
  <div class="wrap">
    <div class="container-fluid">
      <div class="row">
        <div class="header col-md-12">
          @include('frontend.blocks.header')
        </div>
        @yield('content')
        <div class="footer col-md-12">
          @include('frontend.blocks.footer')
        </div>
      </div>
    </div>
    @livewireScripts
</body>

</html>
@include('frontend.partials.scripts')