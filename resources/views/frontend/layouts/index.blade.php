@extends('frontend.master')
@section('title', getTitle())
@section('description', 'TheHungPhat Home')
@section('canonical', Request::url())
@section('image', asset('images/logo.jpg'))
@section('type', 'homepage')
@section('content')
    <div class="slider col-md-12 p-0">
        @include('frontend.blocks.slider')
    </div>
    <div class="top-products col-md-12">
        <div class="block-title col-md-12 text-center py-4">
            <h1>
                <span>TOP PORUDTCS</span>
            </h1>
            <p>The Best of 2022</p>
        </div>
        @include('frontend.blocks.top-product')
        {{-- @include('frontend.blocks.product-list') --}}
    </div>
    <div class="home-articles col-md-12">
        @include('frontend.blocks.home-articles')
    </div>

@endsection
