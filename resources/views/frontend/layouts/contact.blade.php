
@extends('frontend.master')
@section('title', 'TheHungPhat Home')
@section('description', 'TheHungPhat Home')
@section('canonical',(Request::url()))
@section('image',(asset('images/logo.jpg')))
@section('type','homepage')
@section('content')
<div class="page-heading text-center">
    <div class="container">
        <div class="col-md-12">
            <div class="heading-content">
                <h1 class="page-title">LIÊN HỆ</h1>
                <div class="page-breadcrumb">
                    <span>
                        <a href="."> <i class="fas fa-home"></i> Home</a>
                        <i class="fas fa-arrow-right"></i>
                        Contact
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="block-title col-md-12 text-center py-4">
            <h1>
                <span>CÔNG TY THẾ HÙNG PHÁT</span>
            </h1>
            <p>Trụ sở chính: {{ $info->address }}</p>
        </div>
        <div class="contact col-md-12">
			<iframe class="googlemap" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDjmm9Oot3Dxdl7GtwRpQTfskauk53IMSc
			&q={{ $info->address_1 }}" allowfullscreen></iframe>
		</div>
    </div>

    <div class="row">
        <div class="block-title col-md-12 text-center py-4">
            <h1>
                <span>CHI NHÁNH</span>
            </h1>
            <p></p>
        </div>
        <div class="contact col-md-12">
			<iframe class="googlemap" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDjmm9Oot3Dxdl7GtwRpQTfskauk53IMSc
			&q={{ $info->address_2 }}" allowfullscreen></iframe>
		</div>
    </div>
</div>


    <div class="container">
        <form method="get" action="{{route('storeContact')}}">
            @csrf           
            <div class="mb-3 row">
                <label for="inputName" class="col-4 col-form-label">Name</label>
                <div class="col-8">
                    <input type="text" class="form-control" name="inputName" id="inputName" placeholder="Name">
                </div>
            </div>
            <fieldset class="mb-3 row">
                <legend class="col-form-legend col-4">Group name</legend>
                <div class="col-8">
                    you can use radio and checkboxes here
                </div>
            </fieldset>
            <div class="mb-3 row">
                <div class="offset-sm-4 col-sm-8">
                    <button type="submit" class="btn btn-primary">Action</button>
                </div>
            </div>
        </form>
    </div>


@endsection
