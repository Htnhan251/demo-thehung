@extends('frontend.master')
@section('title',$category->translated->name)
@section('description',$category->translated->short_description)
@section('canonical',(Request::url()))
@section('type','product')
@section('content')
    <div class="page-heading text-center">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-content">
                    <h1 class="page-title">{{ $category->translated->name }}</h1>
                    <div class="page-breadcrumb">
                        <span>
                            <a href="."> <i class="fas fa-home"></i> Home</a>
                            <i class="fas fa-arrow-right"></i>
                            <a href="/cate/{{ $category->translated->slug }}">
                                {{ $category->translated->name }}
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="top-products col-md-12">
        <div class="block-title col-md-12 text-center py-4">
            <h1>
                <span>{{$category->translated->name}}</span>
            </h1>
            <p>The Best of 2022</p>
        </div>
        @include('frontend.blocks.product-list')
    </div>
@endsection
