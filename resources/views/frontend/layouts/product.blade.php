@extends('frontend.master')
@section('title',$product->translated->name)
@section('description',$product->translated->short_description)
@section('canonical',(Request::url()))
@section('image',(asset($product->translated->image)))
@section('type','product')
@section('content')
    <div class="page-heading text-center">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-content">
                    <h1 class="page-title">{{ $product->translated->name }}</h1>
                    <div class="page-breadcrumb">
                        <span>
                            <a href="."> <i class="fas fa-home"></i> Home</a> 
                            <i class="fas fa-arrow-right"></i>
                            <a href="/cate/{{ $category->translated->slug }}">
                                {{ $category->translated->name }}
                            </a> 
                            <i class="fas fa-arrow-right"></i>
                            <span>{{ $product->translated->name }}</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-detail bg-white col-md-12">
        @include('frontend.blocks.product-detail')
    </div>
    <div class="container">
        <div class="row py-6">
            <div class="col-md-12 bg-white mt-5 p-5">
                {!! $product->translated->long_description !!}
            </div>
        </div>
    </div>
@endsection
