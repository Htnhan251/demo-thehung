@extends('frontend.master')
@section('title','thehungphat - info')
@section('content')
    <section class="section main-section {{-- parallax-scene-js --}}" style="background:url('{{asset('frontend/images/bg-1-1700x803.jpg')}}') no-repeat center center; background-size:cover;">
        <div class="container bootstrap snippets bootdeys">
            <div class="row" id="user-profile">
                <div class="col-lg-3 col-md-4 col-sm-4">
                    <div class="main-box clearfix">
                        <div class="name-title">
                        <h3> {{Auth::user()->name}} </h3>
                        </div>
                        
                        <div class="profile-status">
                            <i class="fa fa-check-circle"></i> Online
                        </div>
                        <div class="profile-label">
                            <span class="btn-danger">Admin</span>
                        </div>                
                        <div class="profile-message-btn center-block text-center">
                            <form action="#" method="POST">
                                @csrf                  
                                @method('PUT')
                                <div class="input-group-btn img-avatar btn">                                                                     
                                    <a id="lfm" data-input="thumbnail" data-preview="holder" class="a-avatar">
                                        <img id="profileimg" src="https://img.lovepik.com/element/40144/0477.png_300.png" alt="avatar" class="profile-img img-responsive center-block">
                                        <div class="edit-avatar">
                                            <h6 class="btn btn-danger">Ấn Chọn ảnh</h6>                                                                        
                                        </div> 
                                    </a>   

                                    <form action="upload.php" method="post" enctype="multipart/form-data">
                                        Select image to upload:
                                        <input type="file" name="fileToUpload" id="fileToUpload">
                                        <input type="submit" value="Upload Image" name="submit">
                                    </form>

                                </div>     
                                <div class="row">                     
                                    <input id="thumbnail" class="d-none" type="text" name="avatar" value="">                                   
                                    <button class="btn btn-sm btn-danger" type="submit">xác nhận</button>
                                </div>   
                            </form>
                        </div>
        
                        {{-- <div class="profile-stars">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <span>Super User</span>
                        </div> --}}
        
                        <div class="profile-since mt-4">
                            Là thành viên từ: {{date_format(Auth::user()->created_at,"d-m-Y") }}
                        </div>
        
                        {{-- <div class="profile-details">
                            <ul class="fa-ul">
                                <li><i class="fa-li fa fa-truck"></i>Orders: <span>456</span></li>
                                <li><i class="fa-li fa fa-comment"></i>Posts: <span>828</span></li>
                                <li><i class="fa-li fa fa-tasks"></i>Tasks done: <span>1024</span></li>
                            </ul>
                        </div>   --}}
                        
                    </div>
                </div>
        
                <div class="col-lg-9 col-md-8 col-sm-8">
                    <div class="main-box clearfix">
                            <div class="profile-header">
                                <h3><span>Thông tin Cá nhân</span></h3>
                            
                                <button type="button" class="btn btn-primary edit-profile" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                    <i class="fa fa-pencil-square fa-lg"></i> Chỉnh sửa
                                </button>
                            </div>
            
                            <div class="row profile-user-info">                           
                                <div class="col-sm-6">
                                    <hr class="my-4"> 
                                    <div class="profile-user-details clearfix">
                                        <div class="profile-user-details-label">
                                            Họ và tên:
                                        </div>
                                        <div class="profile-user-details-value">
                                            {{Auth::user()->name}}
                                        </div>
                                    </div>
                                    
                                    <div class="profile-user-details clearfix">
                                        <div class="profile-user-details-label">
                                            Địa chỉ:
                                        </div>
                                        <div class="profile-user-details-value">
                                            {{Auth::user()->address}}
                                        </div>
                                    </div>
                                    <div class="profile-user-details clearfix">
                                        <div class="profile-user-details-label">
                                            Email:
                                        </div>
                                        <div class="profile-user-details-value">
                                            {{Auth::user()->email}}
                                        </div>
                                    </div>
                                    <div class="profile-user-details clearfix">
                                        <div class="profile-user-details-label">
                                            Điện thoại:
                                        </div>
                                        <div class="profile-user-details-value">
                                            {{Auth::user()->phone}}
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6 profile-user-info"> 
                                    <hr class="my-4">                         
                                    <li><i class="fa fa-briefcase mx-4" aria-hidden="true"></i>:  #</li>
                                    <li><i class="fa fa-venus-mars mx-4" aria-hidden="true"></i>: #</li>
                                    <li><i class="fa fa-graduation-cap mx-4" aria-hidden="true"></i>: #</li>
                                    <li><i class="fa fa-birthday-cake mx-4" aria-hidden="true"></i> : #</li>
                                    <li><i class="fa fa-book mx-4" aria-hidden="true"></i> : # </li>
                                </div>
                            
                            
                            </div>  
                            <hr class="my-4">  
                
                        <div class="tabs-wrapper profile-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Hoạt động</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Thông báo</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="messages-tab" data-bs-toggle="tab" data-bs-target="#messages" type="button" role="tab" aria-controls="messages" aria-selected="false">Chat</button>
                            </li>
                            </ul>
                            
                            <!-- Tab panes -->
                            <div class="tab-content">
                            <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab"> 
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td class="text-center">
                                                    <i class="fa fa-comment"></i>
                                                </td>
                                                <td>
                                                    Lần thay đổi <span class="text-warning">thông tin cá nhân </span> gần nhất.
                                                </td>
                                                <td>
                                                    {{date_format(Auth::user()->updated_at,'d-m-y')}}
                                                </td>
                                            </tr>             
                                        </tbody>
                                    </table>
                                </div>                    
                            </div>
            
                            <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab"> 
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            ###                
                                        </tbody>
                                    </table>
                                </div> 
                            </div>
            
                            <div class="tab-pane" id="messages" role="tabpanel" aria-labelledby="messages-tab"> 
                                <div class="conversation-wrapper">

                                </div>
                            </div>
                        </div>
                                    
            
                    </div>
                </div>
            </div>
        </div>

        <div class="modal" tabindex="-1">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Modal title</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>
    </section> 
   

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" st>
        <div class="modal-dialog">
            <div class="modal-content pb-4 mt-4 " >
                {{-- start --}}
                <div class="row mt-4">   
                    <h3 class="text-center main-title-form"> THAY ĐỔI THÔNG TIN </h3>
                </div>       
                <div class="container">
                    <div class="container">
                        <form id="user_profile" action="{{route('profile.update',Auth::user()->id)}}" method="POST" class="col-md-6">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label>Họ và tên:</label> <span class="error_msg" id="name"></span>
                                <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}">
                            </div>                            
                           
                            <div class="form-group">
                                <label>Email:</label> <span class="error_msg" id="email"></span>
                                <input type="text" name="email" class="form-control" value="{{Auth::user()->email}}" >
                            </div>                            
                           
                            <div class="form-group">
                                <label>Số điện thoại:</label> <span class="error_msg" id="phone"></span>
                                <input type="text" name="phone" class="form-control" value="{{Auth::user()->phone}}" >
                            </div>
                            
                            <div class="form-group">
                                <label>Địa chỉ:</label> <span class="error_msg" id="address"></span>
                                <input type="text" name="address" class="form-control" value="{{Auth::user()->address}}" >
                            </div>                            
 
                            <div class="form-group mt-2">
                                <button url="{{ route('profile.update',Auth::user()->id) }}" target="{{ route('profile.index') }}" class="btn btn-primary btn-submit">Cập nhật</button>
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Thoát</button>
                            </div>
                        </form>                  
                    </div>
                </div>
            </div>
        </div>
    </div>    

@endsection   