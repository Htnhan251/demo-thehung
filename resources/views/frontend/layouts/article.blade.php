@extends('frontend.master')
@section('title',$article->translated->name)
@section('description',$article->translated->short_description)
@section('canonical',(Request::url()))
@section('image',(asset($article->translated->image)))
@section('type','article')
@section('content')
    <div class="page-heading text-center">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-content">
                    <h1 class="page-title">{{ $article->translated->name }}</h1>
                    <div class="page-breadcrumb">
                        <span>
                            <a href="."> <i class="fas fa-home"></i> Home</a> 
                            <i class="fas fa-arrow-right"></i>
                            <span>{{ $article->translated->name }}</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="article-detail col-md-12">
        @include('frontend.blocks.article-detail')
    </div>
@endsection