@extends('frontend.master')
@section('title','Search: '.$keyword)
@section('description','Search result for '. $keyword)
@section('canonical',(Request::url()))
@section('image',(asset('images/logo.jpg')))
@section('type','search')
@section('content')
    <div class="page-heading text-center">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-content">
                    <h1 class="page-title"> Keyword: {{ $keyword }}</h1>
                    <div class="page-breadcrumb">
                        <span>
                            <a href="."> <i class="fas fa-home"></i> Home</a> 
                            <i class="fas fa-arrow-right"></i>
                            <span>Search</span>
                            <i class="fas fa-arrow-right"></i>
                            <span>{{ $keyword }}</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        @include('frontend.blocks.product-list')
    </div>
@endsection
