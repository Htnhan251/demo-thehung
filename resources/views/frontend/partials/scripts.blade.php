<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
</script>
<script type="text/javascript" src="plugins/flex-slider/jquery.flexslider.js"></script>
<script src="plugins/OwlCarousel2-2.3.4/dist/owl.carousel.min.js"></script>
<script type="text/javascript" src="{{ asset('js/frontend.js') }}"></script>

<script>
    <?php echo htmlspecialchars_decode(getJS()); ?>
    
</script>