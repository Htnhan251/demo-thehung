<title>@yield('title')</title>
<meta name="description" content="@yield('description')" />
<link rel="canonical" href="@yield('canonical')" />
<meta property="og:title" content="@yield('title')" />
<meta property="og:url" content="@yield('canonical')" />
<meta property="og:image" content="@yield('image')" />
<meta property="og:description" content="@yield('description')" />
<meta property="og:site_name" content="@yield('title')" />
<meta property="og:type" content="@yield('type')" />
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="{{ asset('frontend/') }}/">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<link href="plugins/flex-slider/flexslider.css" rel="stylesheet">
<link rel="stylesheet" href="plugins/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
<link rel="stylesheet" href="plugins/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css">
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
<link href="{{ asset('css/frontend.css') }}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<style>
    {{ getCSS() }}
</style>
