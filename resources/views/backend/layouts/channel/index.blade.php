@extends('backend.main')
@section('title', trans('product.product'))
@section('content')
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Channels</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ trans('channel.name')}}</th>
                        <th>{{ trans('channel.slug')}}</th>
                        <th>{{ trans('channel.is_active')}}</th>
                        <th>{{ trans('channel.is_default')}}</th>
                        <th>{{ trans('channel.more')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($channels as $channel)
                        <tr>
                            <td>{{ $channel->id }}</td>
                            <td>{{ $channel->name }}</td>
                            <td>{{ $channel->slug }}</td>
                            <td>{{ $channel->is_active }}</td>
                            <td>{{ $channel->is_default }}</td>
                            <td>
                                @include('backend.partials.list-action-button', [
                                    'href' => route('channel.edit', $channel->id),
                                    'deleteRoute' => route('channel.destroy',$channel->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
