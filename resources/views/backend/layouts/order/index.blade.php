@extends('backend.main')
@section('title', 'Orders')
@section('content')
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">orders</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>SKU</th>
                        <th>Tax %</th>
                        <th>Total</th>
                        <th>Sub total</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->sku }}</td>
                            <td>{{ $order->tax }}</td>
                            <td>{{ $order->total }}</td>
                            <td>{{ $order->sub_total }}</td>
                            <td>{{ $order->status }}</td>
                            <td>
                                @include('backend.partials.list-action-button', [
                                    'href' => route('order.edit', $order->id),
                                    'deleteRoute' => route('order.destroy',$order->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="7">{{ $data->links() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
