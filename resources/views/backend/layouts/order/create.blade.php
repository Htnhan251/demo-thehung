@extends('backend.main')
@section('title', 'Orders')
@section('content')
{!! Form::open(['method' => 'post', 'route' => 'order.store']) !!}
<div class="card">
	<div class="card-header border-0">
		<h3 class="card-title">Create Order</h3>
		<div class="card-tools">
			@include('backend.partials.form-button',['target' => route('order.index')])
		</div>
	</div>
	<div class="card-body">
		@csrf
		@include('backend.layouts.order._form')
	</div>
</div>
{!! Form::close() !!}
@endsection