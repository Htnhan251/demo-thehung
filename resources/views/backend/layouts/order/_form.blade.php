@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      @if(session()->has('channel'))
      <input type="hidden" name="channels[]" value="{{ Session::get('channel')->id}}" />
      @endif
      <div class="form-group">
        <label for="name">SKU</label>
        {!! Form::text('sku', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'SKU' ]) !!}
      </div>
      <div class="form-group">
        <label for="status">Status</label>
        @php
          $status = [0 => 'pending', 1 => 'processing', 2 => 'done', 3 => 'cancled'];
        @endphp
        {!! Form::select('status', $status, $data->status, ['class' => 'form-control form-control-sm']) !!}
      </div>
      <div class="form-group">
        <label for="tax">Tax %</label>
        @php
          $tax = [0 => '0%', 3 => '3%', 10 => '10%'];
        @endphp
        {!! Form::select('tax', $tax, $data->tax, ['class' => 'form-control form-control-sm']) !!}
      </div>
      <div class="form-group">
        <label for="note">Note</label>
        {!! Form::textarea('note', null, ['class'=>'tiny-desc form-control form-control-sm']) !!}
      </div>
    </div>
    <div class="col-md-8">
      <select class="order-products select2" multiple="multiple" name="products[]" data-dropdown-css-class="select2-purple" style="width: 100%;">
        @foreach($products as $product)
          <option value="{{ $product->id }}">{{ $product->translated->name }}</option>
        @endforeach
        
      </select>
        
    </div>
  </div>
</div>