<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="desc">{{ trans('origin.address') }}</label>
                {!! Form::text('address', null, [
                    'class' => 'form-control form-control-sm'
                ]) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="desc">{{ trans('origin.branch_address') }}</label>
                {!! Form::text('branch_address', null, [
                    'class' => 'form-control form-control-sm'
                ]) !!}
            </div>
        </div>
        <div class="col-md-4">
            {!! Form::hidden('locale', Config::get('app.locale'), []) !!}
            <div class="form-group">
                <label for="name">{{ trans('origin.title') }}</label>
                {!! Form::text('name', null, [
                    'class' => 'form-control form-control-sm'
                ]) !!}
            </div>

            <div class="form-group">
                <label for="desc">{{ trans('origin.description') }}</label>
                {!! Form::textarea('desc', null, [
                    'class' => 'tiny-desc form-control form-control-sm'
                ]) !!}
            </div>
            
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="time-title">{{ trans('origin.title') }}</label>
                {!! Form::text('timeTitle', null, [
                    'class' => 'form-control form-control-sm'
                ]) !!}
            </div>

            <div class="form-group">
                <label for="time-desc">{{ trans('origin.description') }}</label>
                {!! Form::textarea('timeDesc', null, [
                    'class' => 'tiny-desc form-control form-control-sm'
                ]) !!}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="info-title">{{ trans('origin.title') }}</label>
                {!! Form::text('infoTitle', null, [
                    'class' => 'form-control form-control-sm'
                ]) !!}
            </div>

            <div class="form-group">
                <label for="info-desc">{{ trans('origin.description') }}</label>
                {!! Form::textarea('infoDesc', null, [
                    'class' => 'tiny-desc form-control form-control-sm'
                ]) !!}
            </div>
        </div>

    </div>
</div>
