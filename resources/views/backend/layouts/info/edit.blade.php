@extends('backend.main')
@section('title', trans('origin.information'))
@section('content')
{!! Form::model($data,['method' => 'put', 'route' => ['info.save']]) !!}
<div class="card">
    <div class="card-header border-0">
        <h3 class="card-title">{{ trans('origin.edit')}}</h3>
        <div class="card-tools">
            @include('backend.partials.form-button',['target' => route('info.show')])
        </div>
    </div>
    <div class="card-body">
        @csrf
        @include('backend.layouts.info._form')
    </div>
</div>
{!! Form::close() !!}
@endsection