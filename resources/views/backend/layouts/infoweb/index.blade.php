@extends('backend.main')
@section('title', trans('product.product'))
@section('content')
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Channels</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tên</th>
                        <th>Địa chỉ 1</th>
                        <th>Địa chỉ 2</th>                       
                        <th>Tiêu đề 1</th>         
                        <th>Tiêu đề 2</th>
                        <th>Tiêu đề 3</th>                       
                        <th>Trạng thái</th>                 
                        <th>{{ trans('origin.more')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $data)
                        <tr>
                            <td>{{ $data->id }}</td>
                            <td>{{ $data->name }}</td>
                            <td>{{ $data->address_1 }}</td>
                            <td>{{ $data->address_2 }}</td>
                            <td>{{ $data->title_1 }}</td>
                            <td>{{ $data->title_2 }}</td>
                            <td>{{ $data->title_3 }}</td>                            
                            <td>{{ $data->is_active==1?"Đã kích hoạt":"Chưa kích hoạt" }}</td>                          
                            <td>
                                <button  type="button" class="btn btn-light btn-sm" data-toggle="modal"
                                data-target="#info-web-{{ $data->id }}">
                                    <i class='fab fa-searchengin'></i>
                                    Xem nhanh
                                </button>
                                @include('backend.partials.list-action-button', [
                                    'href' => route('infoweb.edit', $data->id),
                                    'deleteRoute' => route('infoweb.destroy', $data->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
   
<!-- Modal -->
@foreach ($data as $item)
    <!-- Articles modal-->
    <div class="modal fade" id="info-web-{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ trans('article.info') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <tr>
                            <td>Tên</td>
                            <td>{{$data->name}}</td>
                        </tr>
                        <tr>
                            <td>Địa chỉ 1</td>
                            <td>{{ $data->address_1 }}</td>
                        </tr>
                        <tr>
                            <td>Địa chỉ 2</td>
                            <td>{{ $data->adress_2 }}</td>
                        </tr>
                        <tr>
                            <td>Tiêu đề cột 1</td>
                            <td>{{ $data->title_1 }}</td>
                        </tr>
                        <tr>
                            <td>Nội dung cột 1</td>
                            <td>{!! $data->colum_1 !!}</td>
                        </tr>
                        <tr>
                            <td>Tiêu đề cột 2</td>
                            <td>{{ $data->title_2 }}</td>
                        </tr>
                        <tr>
                            <td>Nội dung cột 2</td>
                            <td>{!! $data->colum_2 !!}</td>
                        </tr>
                        <tr>
                            <td>Tiêu đề cột 3</td>
                            <td>{{ $data->title_3 }}</td>
                        </tr>
                        <tr>
                            <td>Nội dung cột 3</td>
                            <td>{!! $data->colum_3 !!}</td>
                        </tr>
                       
                        <tr>
                            <td> Bản quyền</td>
                            <td>{{ $data->copyright }}</td>
                        </tr>                      
                        <tr>
                            <td>{{ trans('origin.more') }}</td>
                            <td>
                                <a target="_blank" class="btn btn-info btn-sm"
                                    href="{{ route('infoweb.edit', $data->id) }}">
                                    {{ trans('origin.click') }}
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>  
@endforeach



@endsection
