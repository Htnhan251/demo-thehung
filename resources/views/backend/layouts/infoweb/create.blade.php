@extends('backend.main')
@section('title', 'Thêm thông tin')
@section('content')
{!! Form::open(['method' => 'post', 'route' => 'infoweb.store']) !!}
<div class="card">
	<div class="card-header border-0">
		<h3 class="card-title">{{ trans('origin.create') }}</h3>
		<div class="card-tools">
			@include('backend.partials.form-button',['target' => route('infoweb.index')])
		</div>
	</div>
	<div class="card-body">
		@csrf
		@include('backend.layouts.infoweb._form')
	</div>
</div>
{!! Form::close() !!}
@endsection