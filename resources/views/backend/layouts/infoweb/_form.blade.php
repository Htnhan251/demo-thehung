@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="title">Tên Website </label>
        {!! Form::text('name', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'VD: The Wings']) !!}
      </div>  
      <div class="form-group">
        <label for="title">Địa chỉ 1 </label>
        {!! Form::text('address_1', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'VD: The Wings']) !!}
      </div> 
      <div class="form-group">
        <label for="title">Địa chỉ 2 </label>
        {!! Form::text('address_2', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'VD: The Wings']) !!}
      </div>        
    </div>
    <div class="col-md-6">      
        <div class="form-group">
          <label for="title">Copyright </label>
          {!! Form::text('copyright', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'VD: The Wings @Copyright 2023']) !!}
        </div>      
        <div class="form-group">
          <div class="custom-control custom-switch">
            {!! Form::hidden('is_active', 0, []) !!}
            {!! Form::checkbox('is_active', 1, $data->is_active, ['class' => 'custom-control-input', 'id' => 'is_active']) !!}
            <label class="custom-control-label" for="is_active">{{ trans('article.is_active')}}</label>
          </div>
      </div>
    </div>
  </div> 
  <hr>
  <div class="row">      
      <div class="col-md-4">
          {!! Form::hidden('locale', Config::get('app.locale'), []) !!}
          <div class="form-group">
            <label for="name">Cột 1</label>             
          </div>
          <div class="form-group">
            <label for="title">Tiêu đề cột 1 </label>
            {!! Form::text('title_1', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'VD: Website System The Wings']) !!}
          </div> 
          <div class="form-group">
              <label for="colum_1">Nội dung cột 1</label>
              {!! Form::textarea('colum_1', null, [
                  'class' => 'tiny-desc form-control form-control-sm'
              ]) !!}
          </div>          
      </div>

      <div class="col-md-4">
        {!! Form::hidden('locale', Config::get('app.locale'), []) !!}
        <div class="form-group">
          <label for="name">Cột 2</label>             
        </div>
        <div class="form-group">
          <label for="title">Tiêu đề cột 2 </label>
          {!! Form::text('title_2', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'VD: Website System The Wings']) !!}
        </div> 
        <div class="form-group">
            <label for="colum_2">Nội dung cột 2</label>
            {!! Form::textarea('colum_2', null, [
                'class' => 'tiny-desc form-control form-control-sm'
            ]) !!}
        </div>          
      </div>

      <div class="col-md-4">
        {!! Form::hidden('locale', Config::get('app.locale'), []) !!}
        <div class="form-group">
          <label for="name">Cột 3</label>             
        </div>
        <div class="form-group">
          <label for="title">Tiêu đề cột 3 </label>
          {!! Form::text('title_3', null, ['class'=>'form-control form-control-sm', 'placeholder' => 'VD: Website System The Wings']) !!}
        </div> 
        <div class="form-group">
            <label for="colum_3">Nội dung cột 3</label>
            {!! Form::textarea('colum_3', null, [
                'class' => 'tiny-desc form-control form-control-sm'
            ]) !!}
        </div>          
      </div>
      
    </div>
  </div>

</div>
