@extends('backend.main')
@section('title', trans('category.create_category'))
@section('content')
{!! Form::open(['method' => 'post', 'route' => 'category.store']) !!}
<div class="card">
	<div class="card-header border-0">
		<h3 class="card-title">{{ trans('category.create_new_category')}}</h3>
		<div class="card-tools">
			@include('backend.partials.form-button',['target' => route('category.index')])
		</div>
	</div>
	<div class="card-body">
		@csrf
		@include('backend.layouts.category._form')
	</div>
</div>
{!! Form::close() !!}
@endsection