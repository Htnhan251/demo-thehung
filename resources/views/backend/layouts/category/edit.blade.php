@extends('backend.main')
@section('title','Edit Category')
@section('content')
{!! Form::model($data->translated,['method' => 'put', 'route' => ['category.update', $data->id]]) !!}
<div class="card">
    <div class="card-header border-0">
        <h3 class="card-title">{{ trans('category.edit_category')}}</h3>
        <div class="card-tools">
            @include('backend.partials.form-button',['target' => route('category.index')])
        </div>
    </div>
    <div class="card-body">
        @csrf
        @include('backend.layouts.category._form')
    </div>
</div>
{!! Form::close() !!}
@endsection