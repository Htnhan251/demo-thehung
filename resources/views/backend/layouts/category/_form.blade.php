@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      @if(session()->has('channel'))
      <input type="hidden" name="channels[]" value="{{ Session::get('channel')->id}}" />
      @endif
      {!! Form::hidden('locale', Config::get('app.locale'), []) !!}
      <div class="form-group">
        <label for="name">{{ trans('category.category_name')}}</label>
        {!! Form::text('name', null, ['class'=>'form-control form-control-sm', 'placeholder' => trans('category.placeholder_name') ]) !!}
      </div>
      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('is_active', 0, []) !!}
          {!! Form::checkbox('is_active', 1, $data->is_active, ['class' => 'custom-control-input', 'id' => 'is_active']) !!}
          <label class="custom-control-label" for="is_active">Is active</label>
        </div>
      </div>
      <div class="form-group">
        <label for="title">{{ trans('category.title')}}</label>
        {!! Form::text('title', null, ['class'=>'form-control form-control-sm', 'placeholder' => trans('category.placeholder_name') ]) !!}
      </div>
      <div class="form-group">
        <label for="short_description">{{ trans('category.short_description')}}</label>
        {!! Form::textarea('short_description', null, ['class'=>'form-control form-control-sm']) !!}
        <small class="form-text text-muted">This text use for Google SEO</small>
      </div>
    </div>
    <div class="col-md-8">
      
      <div class="form-group">
        <label for="long_description">{{ trans('category.description')}}</label>
        {!! Form::textarea('long_description', null, ['class'=>'form-control form-control-sm tiny-desc' , 'id' => 'desc']) !!}
      </div>
    </div>
  </div>
</div>