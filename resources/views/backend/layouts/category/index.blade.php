@extends('backend.main')
@section('title',trans('category.category'))
@section('content')
<div class="card">
	<div class="card-header border-0">
		<h3 class="card-title">@yield('title')</h3>
		<div class="card-tools">
			<a href="#" class="btn btn-tool btn-sm">
				<i class="fas fa-download"></i>
			</a>
			<a href="#" class="btn btn-tool btn-sm">
				<i class="fas fa-bars"></i>
			</a>
		</div>
	</div>
	<div class="card-body table-responsive p-0">
		<table class="table list table-striped table-valign-middle">
			<thead>
				<tr>
					<th>ID</th>
					<th>{{ trans('category.name')}}</th>
					<th>{{ trans('category.slug')}}</th>
					<th>{{ trans('category.active')}}</th>
					<th>{{ trans('category.actions')}}</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($data as $item)
				<tr>
					<td>{{$item->id}}</td>
					<td>{{$item->translated->name}}</td>
					<td>{{$item->translated->slug}}</td>
					<td>{{$item->is_active}}</td>
					<td>
						<button type="button" class="btn btn-light btn-sm" data-toggle="modal"
							data-target="#category-{{ $item->id }}">
							<i class="fab fa-searchengin"></i>
							Xem nhanh
						</button> 
						@include('backend.partials.list-action-button',[
							'href' => route('category.edit',$item->id),
							'deleteRoute' => route('category.destroy',$item->id)
						])
					</td>
				</tr>	
				@endforeach
				
				<tr><td colspan="5">{{ $data->links() }}</td></tr>
			</tbody>
		</table>
	</div>
</div>


   <!-- Button trigger modal -->

    <!-- Modal -->
    @foreach ($data as $item)
        <!-- Category modal-->
        <div class="modal fade" id="category-{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ trans('category.info') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped">
                            <tr>
                                <td>{{ trans('category.category_name') }}</td>
                                <td>{{ $item->translated->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('category.title') }}</td>
                                <td>{{ $item->translated->title }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.created_at') }}</td>
                                <td>{{ $item->translated->created_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.update_at') }}</td>
                                <td>{{ $item->translated->updated_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('category.short_description') }}</td>
                                <td>{{ $item->translated->short_description }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.image') }}</td>
                                <td><img src="{{ $item->translated->image }}" height="50" width="100" /></td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.more') }}</td>
                                <td>
                                    <a target="_blank" class="btn btn-info btn-sm"
                                        href="{{ route('category.edit', $item->id) }}">
                                        {{ trans('origin.click') }}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    
@endsection