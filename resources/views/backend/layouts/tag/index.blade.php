@extends('backend.main')
@section('title', trans('product.product'))
@section('content')
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Tags</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ trans('tag.name')}}</th>
                        <th>{{ trans('tag.slug')}}</th>
                        <th>{{ trans('tag.is_active')}}</th>
                        <th>{{ trans('tag.is_default')}}</th>
                        <th>{{ trans('tag.more')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tags as $tag)
                        <tr>
                            <td>{{ $tag->id }}</td>
                            <td>{{ $tag->name }}</td>
                            <td>{{ $tag->slug }}</td>
                            <td>{{ $tag->is_active }}</td>
                            <td>{{ $tag->is_default }}</td>
                            <td>
                                @include('backend.partials.list-action-button', [
                                    'href' => route('tag.edit', $tag->id),
                                    'deleteRoute' => route('tag.destroy',$tag->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
