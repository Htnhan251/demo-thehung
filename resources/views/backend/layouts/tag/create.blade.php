@extends('backend.main')
@section('title', trans('product.create_product'))
@section('content')
{!! Form::open(['method' => 'post', 'route' => 'tag.store']) !!}
<div class="card">
	<div class="card-header border-0">
		<h3 class="card-title">{{ trans('origin.create') }}</h3>
		<div class="card-tools">
			@include('backend.partials.form-button',['target' => route('tag.index')])
		</div>
	</div>
	<div class="card-body">
		@csrf
		@include('backend.layouts.tag._form')
	</div>
</div>
{!! Form::close() !!}
@endsection