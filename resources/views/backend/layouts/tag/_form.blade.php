@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label for="name">{{ trans('origin.name')}}</label>
        {!! Form::text('name', null, ['class'=>'form-control form-control-sm', 'placeholder' => trans('product.placeholder_name') ]) !!}
      </div>

      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('is_active', 0, []) !!}
          {!! Form::checkbox('is_active', 1, $data->is_active, ['class' => 'custom-control-input', 'id' => 'is_active']) !!}
          <label class="custom-control-label" for="is_active">{{ trans('origin.is_active')}}</label>
        </div>
      </div>

      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('is_default', 0, []) !!}
          {!! Form::checkbox('is_default', 1, $data->is_default, ['class' => 'custom-control-input', 'id' => 'is_default']) !!}
          <label class="custom-control-label" for="is_default">{{ trans('origin.is_default')}}</label>
        </div>
      </div>

      
    </div>
  </div>
</div>