@extends('backend.main')
@section('title', trans('product.edit_product'))
@section('content')
{!! Form::model($data->translated,['method' => 'put', 'route' => ['product.update', $data->id]]) !!}
<div class="card">
    <div class="card-header border-0">
        <h3 class="card-title">{{ trans('product.edit_product')}}</h3>
        <div class="card-tools">
            @include('backend.partials.form-button',['target' => route('product.index')])
        </div>
    </div>
    <div class="card-body">
        @csrf
        @include('backend.layouts.product._form')
    </div>
</div>
{!! Form::close() !!}
@endsection