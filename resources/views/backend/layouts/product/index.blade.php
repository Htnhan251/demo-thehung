@extends('backend.main')
@section('title', trans('product.product'))
@section('content')

    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Products</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ trans('product.product')}}</th>
                        <th>{{ trans('product.category')}}</th>
                        <th>{{ trans('product.price')}}</th>                       
                        <th>{{ trans('product.image')}}</th>
                        <th>{{ trans('product.more')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->translated ? $product->translated->name : null}}</td>
                            <td>{{ ($product->category_id != null) ? $product->categories->translated->name : null }}
                            </td>
                            <td>{{ myCurrency($product->price)}} Vnđ</td>                           
                            <td>
                                <img src="{{ $product->translated ? $product->translated->image :null }}" height="50" width="100" />
                            </td>
                            <td>
                                <button type="button" class="btn btn-light btn-sm" data-toggle="modal"
                                    data-target="#product-{{ $product->id }}">
                                    <i class="fab fa-searchengin"></i>
                                    Xem nhanh
                                </button> 
                                @include('backend.partials.list-action-button', [
                                    'href' => route('product.edit', $product->id),
                                    'deleteRoute' => route('product.destroy',$product->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="7">{{ $data->links() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>.
    

    
    @foreach ($data as $key => $product)
        <!-- product quickview modal-->
        <div class="modal fade" id="product-{{ $product->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ trans('product.detail') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped">
                            <tr>
                                <td>{{ trans('product.product_name') }}</td>
                                <td>{{ $product->translated ? $product->translated->name : null }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('product.category') }}</td>
                                <td>{{ $product->category_id != null ? $product->categories->translated->name : null }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('product.price') }}</td>
                                <td>{{ myCurrency($product->price)}} Vnđ</td>
                            </tr>
                            <tr>
                                <td>{{ trans('product.title') }}</td>
                                <td>{{ $product->translated ? $product->translated->title: null }}</td>
                            </tr>                           
                            <tr>
                                <td>{{ trans('origin.image') }}</td>
                                <td><img src="{{ $product->translated ? $product->translated->image :null }}" height="50" width="100" /></td>
                            </tr>

                            <tr>
                                <td>{{ trans('origin.edit') }}</td>
                                <td>
                                    <a target="_blank" class="btn btn-info btn-sm"
                                        href="{{ route('product.edit', $product->id) }}">
                                        {{ trans('origin.click') }}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
