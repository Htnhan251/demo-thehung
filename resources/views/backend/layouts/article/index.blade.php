@extends('backend.main')
@section('title', trans('article.article'))
@section('content')
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">{{ trans('article.article')}}</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ trans('article.article')}}</th>
                        <th>{{ trans('article.image')}}</th>
                        <th>{{ trans('article.more')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $article)
                        <tr>
                            <td>{{ $article->id }}</td>
                            <td>{{ $article->translated->name }}</td>
                            <td>
                                <img src="{{ $article->translated->image }}" height="50" width="100" />
                            </td>
                            <td>
                                <button  type="button" class="btn btn-light btn-sm" data-toggle="modal"
                                data-target="#article-{{ $article->id }}">
                                    <i class='fab fa-searchengin'></i>
                                    Xem nhanh
                                </button>
                                @include('backend.partials.list-action-button', [
                                    'href' => route('article.edit', $article->id),
                                    'deleteRoute' => route('article.destroy',$article->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5">{{ $data->links() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


     <!-- Button trigger modal -->

    <!-- Modal -->
    @foreach ($data as $article)
        <!-- Articles modal-->
        <div class="modal fade" id="article-{{ $article->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{ trans('article.info') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped">
                            <tr>
                                <td>{{ trans('article.name') }}</td>
                                <td>{{ $article->translated->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('article.title') }}</td>
                                <td>{{ $article->translated->title }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.created_at') }}</td>
                                <td>{{ $article->translated->created_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.update_at') }}</td>
                                <td>{{ $article->translated->updated_at }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('article.short_description') }}</td>
                                <td>{{ $article->translated->short_description }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.image') }}</td>
                                <td><img src="{{ $article->translated->image }}" height="50" width="100" /></td>
                            </tr>
                            <tr>
                                <td>{{ trans('origin.more') }}</td>
                                <td>
                                    <a target="_blank" class="btn btn-info btn-sm"
                                        href="{{ route('article.edit', $article->id) }}">
                                        {{ trans('origin.click') }}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
