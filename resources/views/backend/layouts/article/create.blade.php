@extends('backend.main')
@section('title', trans('article.create_article'))
@section('content')
{!! Form::open(['method' => 'post', 'route' => 'article.store']) !!}
<div class="card">
	<div class="card-header border-0">
		<h3 class="card-title">{{ trans('article.create_new_article')}}</h3>
		<div class="card-tools">
			@include('backend.partials.form-button',['target' => route('article.index')])
		</div>
	</div>
	<div class="card-body">
		@csrf
		@include('backend.layouts.article._form')
	</div>
</div>
{!! Form::close() !!}
@endsection