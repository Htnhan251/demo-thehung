@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      @if(session()->has('channel'))
      <input type="hidden" name="channels[]" value="{{ Session::get('channel')->id}}" />
      @endif
      {!! Form::hidden('locale', Config::get('app.locale'), []) !!}
      <div class="form-group">
        <label for="name">{{ trans('article.article_name')}}</label>
        {!! Form::text('name', null, ['class'=>'form-control form-control-sm', 'placeholder' => trans('article.placehover_name_article')]) !!}
      </div>
      <div class="form-group">
        <label for="title">{{ trans('article.title')}}</label>
        {!! Form::text('title', null, ['class'=>'form-control form-control-sm', 'placeholder' => trans('article.placehover_title')]) !!}
      </div>
      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('is_active', 0, []) !!}
          {!! Form::checkbox('is_active', 1, $data->is_active, ['class' => 'custom-control-input', 'id' => 'is_active']) !!}
          <label class="custom-control-label" for="is_active">{{ trans('article.is_active')}}</label>
        </div>
      </div>
      <div class="form-group">
        <label for="short_description">{{ trans('article.short_description')}}</label>
        {!! Form::textarea('short_description', null, ['class'=>'form-control form-control-sm']) !!}
        <small class="form-text text-muted">This text use for Google SEO</small>
      </div>
    </div>
    
    <div class="col-md-6">
      <div class="form-group">
        <label for="image">{{ trans('article.image')}}</label>
        {!! Form::text('image', null,['class' => 'form-control form-control-sm', 'id' => 'img']) !!}
        <a data-fancybox data-type="iframe" data-src="{{ url('plugins/filemanager/dialog.php?type=1&field_id=img') }}"
          href="javascript:;">
          {{ trans('article.click_here_browse')}}
        </a>
        <div class="text-center">
          <img id="view-img" src="{{ isset($data->translated->image) ? $data->translated->image : null}}">
        </div>
        <small class="form-text text-muted">{{ trans('article.image_size')}}</small>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label for="description">{{ trans('article.description')}}</label>
        {!! Form::textarea('long_description', null, ['class'=>'form-control form-control-sm tiny-desc' , 'id' => 'desc']) !!}
      </div>
    </div>
  </div>
</div>