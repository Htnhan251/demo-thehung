@extends('backend.main')
@section('title', trans('article.edit_article'))
@section('content')
{!! Form::model($data->translated,['method' => 'put', 'route' => ['article.update', $data->id]]) !!}
<div class="card">
    <div class="card-header border-0">
        <h3 class="card-title">{{ trans('article.edit_article')}}</h3>
        <div class="card-tools">
            @include('backend.partials.form-button',['target' => route('article.index')])
        </div>
    </div>
    <div class="card-body">
        @csrf
        @include('backend.layouts.article._form')
    </div>
</div>
{!! Form::close() !!}
@endsection