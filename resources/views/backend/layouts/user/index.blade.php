@extends('backend.main')
@section('title', trans('user.user'))
@section('content')
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">{{ trans('user.user') }}</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{ trans('user.user_name')}}</th>
                        <th>{{ trans('origin.email')}}</th>
                        <th>{{ trans('user.phone')}}</th>
                        <th>{{ trans('user.is_admin')}}</th>
                        <th>{{ trans('origin.actions')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->is_admin }}</td>
                            <td>
                                @include('backend.partials.list-action-button', [
                                    'href' => route('user.edit', $user->id),
                                    'deleteRoute' => route('user.destroy',$user->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="6">{{ $data->links() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
