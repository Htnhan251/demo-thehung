@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if ($errors)
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger">{{ $error }}</div>
    @endforeach
@endif
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      @if(session()->has('channel'))
        <input type="hidden" name="channels[]" value="{{ Session::get('channel')->id}}" />
      @endif
      <div class="form-group">
        <label for="name">{{ trans('user.user_name')}} *</label>
        {!! Form::text('name', null, ['class'=>'form-control form-control-sm', 'required']) !!}
      </div>

      <div class="form-group">
        <label for="email">{{ trans('origin.email')}} *</label>
        {!! Form::text('email', null, ['class'=>'form-control form-control-sm', 'required']) !!}
      </div>

      <div class="form-group">
        <label for="phone">{{ trans('origin.phone')}} *</label>
        {!! Form::text('phone', null, ['class'=>'form-control form-control-sm', 'required']) !!}
      </div>

      <div class="form-group">
        <label for="address">{{ trans('origin.address')}}</label>
        {!! Form::text('address', null, ['class'=>'form-control form-control-sm']) !!}
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('is_active', 0, []) !!}
          {!! Form::checkbox('is_active', 1, $data->is_active, ['class' => 'custom-control-input', 'id' => 'is_active']) !!}
          <label class="custom-control-label" for="is_active">{{ trans('origin.is_active')}}</label>
        </div>
      </div>

      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('is_admin', 0, []) !!}
          {!! Form::checkbox('is_admin', 1, $data->is_admin, ['class' => 'custom-control-input', 'id' => 'is_admin']) !!}
          <label class="custom-control-label" for="is_admin">{{ trans('user.is_admin')}}</label>
        </div>
      </div>

      @if(request()->routeIS('users.create'))
        <div class="form-group">
          <label for="password">{{ trans('user.password')}} *</label>
          {!! Form::password('password', ['class'=>'form-control form-control-sm', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="confirm_password">{{ trans('user.confirm_password')}} *</label>
          {!! Form::password('confirm_password', ['class'=>'form-control form-control-sm', 'required']) !!}
        </div>
      @else
        <!-- change password form -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#change-password">
          {{ trans('user.change_password')}}
        </button>
      @endif
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label for="profile_photo_path">{{ trans('user.profile_photo_path')}}</label>
        {!! Form::hidden('profile_photo_path', null,['class' => 'form-control form-control-sm', 'id' => 'img']) !!}
        <a data-fancybox data-type="iframe" data-src="{{ url('plugins/filemanager/dialog.php?type=1&field_id=img') }}"
          href="javascript:;">
          (click here to browse...)
        </a>
        <div class="text-center">
          <img id="view-img" src="{{ isset($data->profile_photo_path) ? $data->profile_photo_path : null}}">
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="form-group">
        <label for="note">{{ trans('origin.note')}}</label>
        {!! Form::textarea('note', null, ['class'=>'form-control form-control-sm tiny-desc' , 'id' => 'desc']) !!}
      </div>
    </div>
  </div>
</div>