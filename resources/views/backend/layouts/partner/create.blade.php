@extends('backend.main')
@section('title', trans('partner.create_partner'))
@section('content')
{!! Form::open(['method' => 'post', 'route' => 'partner.store']) !!}
<div class="card">
	<div class="card-header border-0">
		<h3 class="card-title">{{ __('Create new Partner')}}</h3>
		<div class="card-tools">
			@include('backend.partials.form-button',['target' => route('partner.index')])
		</div>
	</div>
	<div class="card-body">
		@csrf
		@include('backend.layouts.product._form')
	</div>
</div>
{!! Form::close() !!}
@endsection