@extends('backend.main')
@section('title', trans('partner.edit_partner'))
@section('content')
{!! Form::model($data->translated,['method' => 'put', 'route' => ['partner.update', $data->id]]) !!}
<div class="card">
    <div class="card-header border-0">
        <h3 class="card-title">{{ trans('partner.edit_partner')}}</h3>
        <div class="card-tools">
            @include('backend.partials.form-button',['target' => route('partner.index')])
        </div>
    </div>
    <div class="card-body">
        @csrf
        @include('backend.layouts.product._form')
    </div>
</div>
{!! Form::close() !!}
@endsection