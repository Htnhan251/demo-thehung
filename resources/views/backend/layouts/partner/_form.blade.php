@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      @if(session()->has('channel'))
      <input type="hidden" name="channels[]" value="{{ Session::get('channel')->id}}" />
      @endif
      {!! Form::hidden('locale', Config::get('app.locale'), []) !!}
      <div class="form-group">
        <label for="name">{{ trans('product.product_name')}}</label>
        {!! Form::text('name', null, ['class'=>'form-control form-control-sm', 'placeholder' => trans('product.placeholder_name') ]) !!}
      </div>
      <div class="form-group">
        <label for="title">{{ trans('product.title')}}</label>
        {!! Form::text('title', null, ['class'=>'form-control form-control-sm', 'placeholder' => trans('product.placeholder_title') ]) !!}
      </div>
      <div class="form-group">
        <label for="sku">{{ trans('product.sku')}}</label>
        {!! Form::text('sku', $data->sku, ['class'=>'form-control form-control-sm', 'placeholder' => trans('product.placeholder_sku')])
        !!}
      </div>
      <div class="form-group">
        <label for="price">{{ trans('product.price')}}</label>
        {!! Form::number('price', $data->price, ['class'=>'form-control form-control-sm', 'placeholder' => trans('product.placeholder_price')]) !!}
      </div>
      <div class="form-group">
        <label for="qty">{{ trans('product.quantity')}}</label>
        {!! Form::number('qty', $data->qty, ['class'=>'form-control form-control-sm']) !!}
      </div>
      
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="category_id">{{ trans('product.category')}}</label>
        {!! Form::select('category_id', $categories, $data->category_id, ['class' => 'form-control form-control-sm']) !!}
      </div>
      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('is_active', 0, []) !!}
          {!! Form::checkbox('is_active', 1, $data->is_active, ['class' => 'custom-control-input', 'id' => 'is_active']) !!}
          <label class="custom-control-label" for="is_active">{{ trans('product.is_active')}}</label>
        </div>
      </div>
      <div class="form-group">
        <div class="custom-control custom-switch">
          {!! Form::hidden('in_stock', 0, []) !!}
          {!! Form::checkbox('in_stock', 1, $data->in_stock, ['class' => 'custom-control-input', 'id' => 'in_stock']) !!}
          <label class="custom-control-label" for="in_stock">{{ trans('product.in_stock')}}</label>
        </div>
      </div>
      <div class="form-group">
        <label for="short_description">{{ trans('product.short_description')}}</label>
        {!! Form::textarea('short_description', null, ['class'=>'form-control form-control-sm']) !!}
        <small class="form-text text-muted">This text use for Google SEO</small>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="image">{{ trans('product.image')}}</label>
        {!! Form::hidden('image', null,['class' => 'form-control form-control-sm', 'id' => 'img']) !!}
        <a data-fancybox data-type="iframe" data-src="{{ url('plugins/filemanager/dialog.php?type=1&field_id=img') }}"
          href="javascript:;">
          (click here to browse...)
        </a>
        <div class="text-center">
          <img id="view-img" src="{{ isset($data->translated->image) ? $data->translated->image : null}}">
        </div>
        <small class="form-text text-muted">{{ trans('product.image_size')}}</small>
        @if($data->images)
          <div class="product-images">
            @foreach($data->images as $image)
              <img class="images-thumb" src="{{ $image->path }}" width="80" height="80" />
            @endforeach
          </div>
        @endif
        <div class="images-input"></div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="special_price">{{ trans('product.special_price')}}</label>
        {!! Form::number('special_price', $data->special_price, ['class'=>'form-control form-control-sm']) !!}
      </div>
      <div class="form-group">
        <label for="special_price_start">{{ trans('product.special_price_start')}}</label>
        {!! Form::date('special_price_start', $data->special_price_start , ['class' => 'form-control form-control-sm']) !!}
      </div>
      <div class="form-group">
        <label for="special_price_start">{{ trans('product.special_price_end')}}</label>
        {!! Form::date('special_price_end', $data->special_price_end , ['class' => 'form-control form-control-sm']) !!}
      </div>
    </div>
    <div class="col-md-8">
      <div class="form-group">
        <label for="long_description">{{ trans('product.description')}}</label>
        {!! Form::textarea('long_description', null, ['class'=>'tiny-desc form-control form-control-sm']) !!}
      </div>
    </div>
  </div>
</div>