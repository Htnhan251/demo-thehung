@extends('backend.main')
@section('title', 'Trang Đối Tác')
@section('content')
    <div class="card">
        <div class="card-header border-0">
            <h3 class="card-title">Products</h3>
            <div class="card-tools">
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-download"></i>
                </a>
                <a href="#" class="btn btn-tool btn-sm">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Company Name</th>
                        <th>Short Name</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Tax Number</th>
                        <th>More</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $partner)
                        <tr>
                            <td>{{ $partner->id }}</td>
                            <td>{{ $partner->name }}</td>
                            <td>{{ $partner->company_name }}</td>
                            <td>{{ $partner->short_name }}</td>
                            <td>{{ $partner->phone }}</td>
                            <td>{{ $partner->email }}</td>
                            <td>{{ $partner->address }}</td>
                            <td>{{ $partner->tax_no }}</td>
                            <td>{{ $partner->sku }}</td>
                            
                            <td>
                                @include('backend.partials.list-action-button', [
                                    'href' => route('partner.edit', $partner->id),
                                    'deleteRoute' => route('partner.destroy',$partner->id)
                                ])
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="10">{{ $data->links() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection
