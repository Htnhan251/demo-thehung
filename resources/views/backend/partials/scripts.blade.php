<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard3.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
{{-- <script src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script> --}}
<script src="{{ asset('plugins/fancybox-master/dist/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('plugins/filemanager/responsive_fm.js') }}"></script>
<script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>


<script>
    <?php echo htmlspecialchars_decode(getJS()); ?>
</script>
