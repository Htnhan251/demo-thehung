<a class="btn btn-sm btn-info" href="{{ $href }}" >
    <i class="fas fa-pen"></i> {{ trans('origin.edit')}}
</a>
<button type="button" class="btn btn-sm btn-light delete" route="{{ $deleteRoute }}">
    <i class="fas fa-trash-alt"></i> {{ trans('origin.delete')}}
</button>