<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a class="brand-link">
        <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            {{-- <img src="{{ asset(getLogo())}}" alt="AdminLTE Logo" --}} class="brand-image img-circle elevation-3" {{-- {{ dd(session()->all()) }} --}} {{-- data-toggle="modal" data-target="#logoImage" --}}
            style="opacity: .8">
        <span class="brand-text font-weight-light" data-toggle="modal" data-target="#logoImage">AdminLTE </span>
    </a>

    {{-- {{ dd(config('logo-image.logo')) }} --}}


    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="/images/long-avatar.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
            <div>
                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}" x-data>
                    @csrf

                    <x-jet-dropdown-link href="{{ route('logout') }}" @click.prevent="$root.submit();">
                        {{ __('Log Out') }}
                    </x-jet-dropdown-link>
                </form>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                    aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
                <li
                    class="nav-item {{ request()->routeIS('category.index') || request()->routeIS('category.index') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('category.index') || request()->routeIS('category.index') ? 'active' : null }}">
                        <i class="fas fa-folder"></i>
                        <p>
                            {{ trans('origin.category') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('category.index') ? 'item-active' : null }}">
                            <a href="{{ route('category.index') }}" class="nav-link">
                                <i class="fas fa-list-ul nav-icon"></i>
                                <p>{{ trans('origin.list_categories') }}</p>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ request()->routeIS('category.create') ? 'item-active' : null }}">
                            <a href="{{ route('category.create') }}" class="nav-link">
                                <i class="fas fa-folder-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li> --}}
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIS('product.index') || request()->routeIS('product.create') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('product.index') || request()->routeIS('product.create') ? 'active' : null }}">
                        <i class="fab fa-product-hunt"></i>
                        <p>
                            {{ trans('origin.product') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('product.index') ? 'item-active' : null }}">
                            <a href="{{ route('product.index') }}" class="nav-link">
                                <i class="fas fa-cube nav-icon"></i>
                                <p>{{ trans('origin.list_products') }}</p>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ request()->routeIS('product.create') ? 'item-active' : null }}">
                            <a href="{{ route('product.create') }}" class="nav-link">
                                <i class="fas fa-folder-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li> --}}
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIS('article.index') || request()->routeIS('article.index') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('article.index') || request()->routeIS('article.index') ? 'active' : null }}">
                        <i class="fas fa-file-alt"></i>
                        <p>
                            {{ trans('origin.article') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('article.index') ? 'item-active' : null }}">
                            <a href="{{ route('article.index') }}" class="nav-link">
                                <i class="fas fa-bars nav-icon"></i>
                                <p>{{ trans('origin.list_articles') }}</p>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ request()->routeIS('article.create') ? 'item-active' : null }}">
                            <a href="{{ route('article.create') }}" class="nav-link">
                                <i class="fas fa-file-signature nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li> --}}
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIS('user.index') || request()->routeIS('user.create') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('user.index') || request()->routeIS('user.create') ? 'active' : null }}">
                        <i class="fas fa-users"></i>
                        <p>
                            {{ trans('origin.user') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('user.index') ? 'item-active' : null }}">
                            <a href="{{ route('user.index') }}" class="nav-link">
                                <i class="fas fa-user-friends nav-icon"></i>
                                <p>{{ trans('origin.list_user') }}</p>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->routeIS('user.create') ? 'item-active' : null }}">
                            <a href="{{ route('user.create') }}" class="nav-link">
                                <i class="fas fa-user-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                {{-- <li class="nav-item">
            <a href="{{ route('info.show') }}" class="nav-link {{ request()->routeIS('info.show') ? 'active' : null }}">
              <i class="fas fa-info-circle"></i>
              <p>
                {{ trans('origin.information')}}
              </p>
            </a>
          </li> --}}

                <li
                    class="nav-item {{ request()->routeIS('infoweb.index') || request()->routeIS('infoweb.create') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('infoweb.index') || request()->routeIS('infoweb.create') ? 'active' : null }}">
                        <i class="fas fa-info-circle"></i>
                        <p>
                            Thông tin Website
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('infoweb.index') ? 'item-active' : null }}">
                            <a href="{{ route('infoweb.index') }}" class="nav-link">
                                <i class="fas fa-list-ul nav-icon"></i>
                                <p>List Info Website</p>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->routeIS('infoweb.create') ? 'item-active' : null }}">
                            <a href="{{ route('infoweb.create') }}" class="nav-link">
                                <i class="fas fa-user-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIS('channel.index') || request()->routeIS('channel.create') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('channel.index') || request()->routeIS('channel.create') ? 'active' : null }}">
                        <i class="fas fa-stream"></i>
                        <p>
                            Channels
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('channel.index') ? 'item-active' : null }}">
                            <a href="{{ route('channel.index') }}" class="nav-link">
                                <i class="fas fa-list-ul nav-icon"></i>
                                <p>List Channels</p>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->routeIS('channel.create') ? 'item-active' : null }}">
                            <a href="{{ route('channel.create') }}" class="nav-link">
                                <i class="fas fa-user-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIS('partner.index') || request()->routeIS('partner.create') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('partner.index') || request()->routeIS('partner.create') ? 'active' : null }}">
                        <i class="fas fa-stream"></i>
                        <p>
                            Partners
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('partner.index') ? 'item-active' : null }}">
                            <a href="{{ route('partner.index') }}" class="nav-link">
                                <i class="fas fa-list-ul nav-icon"></i>
                                <p>List Partners</p>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->routeIS('partner.create') ? 'item-active' : null }}">
                            <a href="{{ route('partner.create') }}" class="nav-link">
                                <i class="fas fa-user-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li
                    class="nav-item {{ request()->routeIS('order.index') || request()->routeIS('order.create') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('order.index') || request()->routeIS('order.create') ? 'active' : null }}">
                        <i class="fas fa-stream"></i>
                        <p>
                            orders
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item {{ request()->routeIS('order.index') ? 'item-active' : null }}">
                            <a href="{{ route('order.index') }}" class="nav-link">
                                <i class="fas fa-list-ul nav-icon"></i>
                                <p>List orders</p>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->routeIS('order.create') ? 'item-active' : null }}">
                            <a href="{{ route('order.create') }}" class="nav-link">
                                <i class="fas fa-user-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>


                <li
                    class="nav-item {{ request()->routeIS('file.index') || request()->routeIS('file.index') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('file.index') || request()->routeIS('file.create') ? 'active' : null }}">
                        <i class="fas fa-stream"></i>
                        <p>
                            File
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item item-active">
                            <a href="{{ route('file.index') }}" class="nav-link">
                                <i class="fas fa-list-ul nav-icon"></i>
                                <p>List File</p>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ request()->routeIS('order.create') ? 'item-active' : null }}">
                            <a href="{{ route('order.create') }}" class="nav-link">
                                <i class="fas fa-user-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li> --}}
                    </ul>
                </li>
                <li class="nav-item {{ request()->routeIs('slide.index') ? 'menu-open' : null }}">
                    <a href="#"
                        class="nav-link {{ request()->routeIS('slide.index') || request()->routeIS('slide.create') ? 'active' : null }}">
                        <i class="fas fa-stream"></i>
                        <p>
                            Slide
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item item-active">
                            <a href="{{ route('slide.index') }}" class="nav-link">
                                <i class="fas fa-list-ul nav-icon"></i>
                                <p>List Slide</p>
                            </a>
                        </li>
                        {{-- <li class="nav-item {{ request()->routeIS('order.create') ? 'item-active' : null }}">
                            <a href="{{ route('order.create') }}" class="nav-link">
                                <i class="fas fa-user-plus nav-icon"></i>
                                <p>{{ trans('origin.create_new') }}</p>
                            </a>
                        </li> --}}
                    </ul>
                </li>
                <li class="nav-item ">
                    <a href="{{ route('webinfo.index') }}" class="nav-link ">
                        <i class="fas fa-list-ul nav-icon"></i>
                        <p>Settings</p>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
{{-- Model Image Logo --}}
{{-- <div class="modal fade" id="logoImage" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    tabindex="-1" wire:ignore.self>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Change Logo Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('change.logo') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <input type="file" name="logo-image">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div> --}}
