<div>
    <form action="{{ route('search') }}" method="POST" class="d-flex" role="search">
        @csrf
        <input wire:model="search" name='keyword' class="form-control me-2" type="search" placeholder="Search"
            aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
    <ul class="searchbox__result">
        @if(count($products) > 0)
            @foreach ($products as $item)
                <div class="searchbox__item-list">
                    <img src="{{ $item->translated->image }}" alt="" srcset="">
                    <div>
                        <li> <strong><a href="/detail/{{ $item->slug }}"> {{ $item->translated->name }}</strong> </a>
                        </li>
                        <div class="price">
                            {{ number_format($item->price, 0, ',', '.') == 0 ? 'Liên hệ' : number_format($item->price, 0, ',', '.') . ' ' . 'VND' }}
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        {{ $noti }}
    </ul>
</div>
