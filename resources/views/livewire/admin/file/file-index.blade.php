<div>

    <div class="card">
        <div class="card-body table-responsive p-10">
            <div class="upload" wire:ignore.self>
                <form class="form-file" wire:submit.prevent="save()">
                    <input type="file" wire:model="photo" class="btn">
                    @error('photo')
                        <span class="error">{{ $message }}</span>
                    @enderror
                    <button type="submit">Save Photo</button>
                </form>
            </div>
        </div>

    </div>

    <div class="card index-list">
        <div class="card-header border-0">
            <h3 class="card-title">Danh sách file</h3>
            <div class="card-tools">
                <a class="btn btn-tool btn-sm" href="#">
                    <i class="fas fa-download"></i>
                </a>
                <a class="btn btn-tool btn-sm" href="#">
                    <i class="fas fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="card-body table-responsive p-10">
            <div class="row">

                @foreach ($data as $file)
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-3 p-4">
                        <div class="card-body-file">
                            <div class="file-image">
                                <img src="{{ asset($file->path) }}" title="{{ $file->name }}" alt="Card image cap"
                                    alt="{{ $file->name }}">
                            </div>
                            <div class="file-name bg-white">
                                {{ $file->name }}
                            </div>
                            <div class="file-action bg-white text-center pb-2">
                                <button class="btn btn-delete" data-toggle="modal" data-target="#deleteConfirmModel"
                                    type="button"
                                    wire:click="deleteConfirm({{ $file->id }}, '{{ $file->path }}')">
                                    <i class="fa-solid fa-trash mr-2"></i> Delete
                                </button>
                                <button class="btn btn-edit" data-toggle="modal" data-target="#editModal" type="button"
                                    wire:click="edit({{ $file->id }})">
                                    <i class="fa-solid fa-pen-to-square"></i> Edit
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
    <div class="admin-file__paginate">
        {{ $data->links('livewire.paginate.custom-pagination-links') }}
    </div>
    {{-- Modal edit  --}}
    @include('livewire.admin.file.edit-modal')

    <!-- Modal delete -->
    @include('livewire.admin.file.delete-modal')
</div>
