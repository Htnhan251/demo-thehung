@foreach ($category as $item)
    <div class="modal fade" id="modalView-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        wire:submit.prevent="" wire:ignore.self data-backdrop="true" data-keyboard="false" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <h5 class="modal-title" id="exampleModalLabel">Nội dung code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body table-responsive p-0">
                        <table class="table list table-striped ">
                            <tbody>
                                <tr>
                                    <td>Tên bài viết</td>
                                    <td>{{ $item->translate('name') }}</td>
                                </tr>
                                <tr>
                                    <td>Tiêu đề</td>
                                    <td>{{ $item->translate('title') }}</td>
                                </tr>
                                <tr>
                                    <td>Ngày tạo</td>
                                    <td>{{ $item->translate('created_at') }}</td>
                                </tr>
                                <tr>
                                    <td>Ngày sửa</td>
                                    <td>{{ $item->translate('updated_at') }}</td>
                                </tr>
                                <tr>
                                    <td>Mô tả ngắn</td>
                                    <td>{{ $item->translate('short_description') }}</td>
                                </tr>
                                <tr>
                                    <td>Hình ảnh</td>
                                    <td>
                                        <img src="{{asset($item->translate('image'))}}" width="250px" height="150px" alt="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endforeach
