<div>
    <div class="card">
        <div class="card-header border-0">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="d-flex justify-content-between align-items-center">
                <h3 class="card-title">Danh sách danh mục</h3>
                <button wire:click="setLocale()" class="btn btn btn-primary btn-sm bg-primary ">
                    <a href="{{ route('category.create') }}"> Tạo danh mục</a>
                </button>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table list table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th style="text-align: left">ID</th>
                        <th style="text-align: center">Tiêu đề </th>
                        <th style="text-align: center">Slug</th>
                        <th style="text-align: center">Tình trạng</th>
                        <th style="text-align: center">Hành động </th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($category as $item)
                        <tr>
                            <td style="text-align: left">{{ $item->id }}</td>
                            <td style="text-align: center">{{ $item->translate('name') }}</td>
                            <td style="text-align: center">{{ $item->translate('slug') }}</td>
                            <td style="text-align: center">{!! $item->is_active !!}</td>
                            <td>
                                <button class="btn btn-sm btn-light " data-toggle="modal"
                                    data-target="#modalView-{{ $item->id }}">Xem nội dung code</button>
                                <button class="btn btn-sm btn-info">
                                    <a href="{{ route('category.edit', $item->id) }}">Sửa</a>
                                </button>
                                <button class="btn btn-sm btn-light " wire:click="deleteComfirm({{ $item->id }})"
                                    data-toggle="modal" data-target="#deleteConfirmModel">Xóa</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="category_paginate">
        {{ $category->links('livewire.paginate.custom-pagination-links') }}
    </div>
    {{-- view modal --}}
    @include('livewire.admin.category.view-modal')
    {{-- delete --}}
    @include('livewire.admin.category.delete-modal')
</div>
