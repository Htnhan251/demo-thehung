<div>
    <div class="card">
        <div class="card-header border-0">
            <div class="d-flex justify-content-between align-items-center">
                <h3 class="card-title">Code CSS/Javascript</h3>
                <button class="btn btn btn-primary btn-sm bg-primary " wire:click="resetData()" data-toggle="modal"
                    data-target="#modalCreate">Thêm
                    code</button>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table list table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th style="text-align: left">ID</th>
                        <th style="text-align: center">Tiêu đề </th>
                        <th style="text-align: center">Loại</th>
                        <th style="text-align: center">Tình trạng</th>
                        <th style="text-align: center">Hành động </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td style="text-align: left">{{ $item->id }}</td>
                            <td style="text-align: center">{{ $item->title }}</td>
                            <td style="text-align: center">{!! isType($item->type) !!}</td>
                            <td style="text-align: center">{!! isStatus($item->is_active) !!}</td>
                            <td>
                                <button class="btn btn-sm btn-light " wire:click="view({{ $item->id }})"
                                    data-toggle="modal" data-target="#modalView">Xem nội dung code</button>
                                <button class="btn btn-sm btn-info" wire:click="edit({{ $item->id }})"
                                    data-toggle="modal" data-target="#modalEdit">Sửa</button>
                                <button class="btn btn-sm btn-light " wire:click="deleteComfirm({{ $item->id }})"
                                    data-toggle="modal" data-target="#deleteConfirmModel">Xóa</button>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

    {{--  --}}
    @include('livewire.admin.code.create-modal')

    {{-- view --}}
    @include('livewire.admin.code.view-modal')
    {{-- edit --}}
    @include('livewire.admin.code.edit-modal')

    {{-- dlete --}}
    @include('livewire.admin.code.delete-modal')
</div>
