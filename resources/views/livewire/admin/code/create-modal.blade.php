<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    wire:submit.prevent="store()" wire:ignore.self data-backdrop="true" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <h5 class="modal-title" id="exampleModalLabel">Tạo thông tin web</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tiêu đề code </label>
                        <input type="text" wire:model="title" class="form-control"
                            placeholder="Mời nhập tiêu đề code  ">
                    </div>
                    <div class="form-group">
                        <label>Nội dung code </label>
                        <textarea class="form-control rounded" wire:model="content" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Chọn loại code </label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" wire:model="type" value="0">
                            <label class="form-check-label">
                                Css
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" wire:model="type" value="1">
                            <label class="form-check-label">
                                Javascript
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tình trạng </label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" wire:model="is_active" value="0">
                            <label class="form-check-label">
                                Vô hiệu hóa
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" wire:model="is_active" value="1"
                                checked>
                            <label class="form-check-label">
                                Kích hoạt
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-top-0 d-flex justify-content-center">
                    {{-- <input type="button" class="btn btn-primary bg-primary" wire:click="store()"
                            value="Add"> --}}
                    <button type="submit" class="btn btn-primary bg-primary">Thêm</button>
                </div>
            </form>
        </div>
    </div>
</div>
