<div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    wire:submit.prevent="store()" wire:ignore.self data-backdrop="true" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <h5 class="modal-title" id="exampleModalLabel">Nội dung code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {{-- <label>Nội dung code </label> --}}
                    <textarea class="form-control rounded " readonly wire:model="content" rows="3">
                    {{ $content }}
                </textarea>
                </div>
            </div>
        </div>
    </div>
</div>
