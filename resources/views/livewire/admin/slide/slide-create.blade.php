<div>
    <div class="content">
        <div class="container">
            <div class="content__header d-flex">
                <h1>Tạo Slide</h1>
                <div class="content-button">
                    <button class="btn btn btn-primary btn-sm bg-primary " wire:click="store()" data-toggle="modal"
                        data-target="#modalCreate">Lưu</button>
                </div>
            </div>
            <div class="content__body pt-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            {{-- <div class="content-title card-title">
                                Tạo slide
                            </div> --}}
                            <div class="content-body">
                                <form class="form-horizontal" action="">
                                    <div class="form-content">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tiêu đề slide</label>
                                            <div class="col-md-4" wire:model="title">
                                                <input class="form-control" type="text" value="">
                                                @error('title')
                                                    <span style="color: red">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Chọn ảnh </label>
                                            <a href="#" data-toggle="modal" data-target="#fileModal">(Bấm vào để
                                                chọn ảnh)</a>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Ảnh đã chọn: </label>
                                            <div class="select-image-list">
                                                <div class="row">
                                                    @if (count($listImages) > 0)
                                                        @foreach ($listImages as $key => $item)
                                                            <div class="text-center">
                                                                <div
                                                                    class="image-item select-image-items slide-image-select">
                                                                    <img src="{{ asset($item) }}" alt="">
                                                                </div>
                                                                <a href="#" onclick="return false;"
                                                                    wire:click="RemoveImageToSlide({{ $key }})">Remove</a>
                                                            </div>
                                                        @endforeach
                                                    @endif


                                                    {{-- <div class="text-center">
                                                        <div class="image-item select-image-items slide-image-select">
                                                            <img src="{{ asset('images/logo.jpg') }}" alt="">
                                                        </div>
                                                        <a href="#">Remove</a>
                                                    </div>
                                                    <div class="text-center">
                                                        <div class="image-item select-image-items slide-image-select">
                                                            <img src="{{ asset('storage/images/banner-blog.png') }}"
                                                                alt="">
                                                        </div>
                                                        <a href="#">Remove</a>
                                                    </div>
                                                    <div class="text-center">
                                                        <div class="image-item select-image-items slide-image-select">
                                                            <img src="{{ asset('images/logo.jpg') }}" alt="">
                                                        </div>
                                                        <a href="#">Remove</a>
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{--  --}}
    @include('livewire.admin.file.file-modal')
</div>
