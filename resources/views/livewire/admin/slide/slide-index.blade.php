<div>
    <div class="card">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="card-body table-responsive p-10">
            <div class="upload" style="float: left" wire:ignore.self>
                {{-- <form class="form-file" wire:submit.prevent="save()">
                    <input type="file" wire:model="photo" class="btn">
                    @error('photo')
                        <span class="error">{{ $message }}</span>
                    @enderror
                    <button type="submit">Save Photo</button>
                </form> --}}
            </div>
            <button class="btn btn btn-primary bg-primary " style="float: right">
                <a href="{{ route('slide.create') }}">
                    Tạo Slide
                </a>
            </button>
        </div>

    </div>

    <div class="card index-list">
        <div class="card-header border-0">
            <h3 class="card-title">Danh sách Slide</h3>
            <div class="card-tools">
                {{-- <a class="btn btn-tool btn-sm" href="#">
                    <i class="fas fa-download"></i>
                </a>
                <a class="btn btn-tool btn-sm" href="#">
                    <i class="fas fa-bars"></i>
                </a> --}}
            </div>
        </div>

        <div class="card card-body table-responsive">
            <table class="table table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th style="text-align: center">Tiêu đề </th>
                        {{-- <th style="text-align: center">Nội dung</th> --}}
                        <th style="text-align: center">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($slides as $item)
                        <tr>
                            <th>{{ $item->id }}</th>
                            <td style="text-align: center">{{ $item->title }}</td>
                            {{-- <td style="text-align: center">{{ $item->content }}</td> --}}
                            <td style="text-align: center">
                                {{-- <button class="btn btn-sm btn-light ">Xem nội dung slide</button> --}}
                                <button class="btn btn-sm btn-info">
                                    <a href="{{ route('slide.edit', $item->id) }}">
                                        Sửa
                                    </a>
                                </button>
                                <button class="btn btn-sm btn-light " wire:click="deleteComfirm({{ $item->id }})"
                                    data-toggle="modal" data-target="#deleteConfirmModel">Xóa</button>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        {{ $slides->links('livewire.paginate.custom-pagination-links') }}
        {{-- <div class="card-body table-responsive p-10">
            <div class="row">
                @foreach ($slides as $slide)
                    <div class="col-sm-12 col-md-4 col-lg-4 col-xl-3 p-4">
                        <div class="card-body-file">
                            <div class="file-image">
                                <img src="{{ asset($slide->content) }}" alt="Card image cap">
                            </div>
                            <div class="file-name bg-white">
                                {{ $slide->title }}
                            </div>
                            <div class="file-action bg-white text-center pb-2">
                                <button class="btn btn-delete" data-toggle="modal" data-target="#deleteConfirmModel"
                                    type="button" wire:click="deleteConfirm()">
                                    <i class="fa-solid fa-trash mr-2"></i> Delete
                                </button>
                                <button class="btn btn-edit" data-toggle="modal" data-target="#editModal" type="button"
                                    wire:click="edit()">
                                    <i class="fa-solid fa-pen-to-square"></i> Edit
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div> --}}

    </div>
    {{-- modal --}}
    @include('livewire.admin.slide.add-slide')


    @include('livewire.admin.slide.choose-file-modal')

    @include('livewire.admin.slide.delete-modal')
</div>
