<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        wire:submit.prevent="store()" wire:ignore.self data-backdrop="true" data-keyboard="false" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header border-bottom-0">
                    <h5 class="modal-title" id="exampleModalLabel">Create Slide</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" wire:model="title" class="form-control" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label>Chọn ảnh thêm vào slide</label>
                            <input type="button" class="form-control" data-toggle="modal" data-target="#fileModal"
                                value="Choose Image">
                        </div>
                        <div class="form-group">
                            <label>Hình đã chọn</label>
                            <div class="row">
                                @if (count($arr) > 0)
                                    @foreach ($arr as $key => $item)
                                        <div class="col-6 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="margin-top: 20px">
                                            <div class="select-image-items slide-image-select">
                                                <img src="{{ asset($item) }}" width="100%" />
                                                <div class="slide-image-button">
                                                    <button class="btn btn-danger btn-sm" type="button"
                                                        wire:click="RemoveImageToSlide({{ $key }})">
                                                        <i class="mr-2"></i> Xóa
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer border-top-0 d-flex justify-content-center">
                        {{-- <input type="button" class="btn btn-primary bg-primary" wire:click="store()"
                            value="Add"> --}}
                        <button type="submit" class="btn btn-primary bg-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>