<div>
    <div class="content">
        <div class="container-fluid">
            <div class="category__create-form">
                <form wire:ignore.self class="form-content" wire:submit.prevent action="">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-button">
                        <button class="btn btn btn-primary btn-sm bg-primary" wire:click="store()">Lưu</button>
                        <button class="btn btn-light btn-outline-dark  btn-sm">Exit</button>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="card">
                                <h1>Title</h1>
                                {{-- <input type="hidden" wire:model="locale" value="{{ Config::get('app.locale') }}" /> --}}
                                <div class="form-group">
                                    <label class="control-label">Tên bài viết</label>
                                    <input class="form-control" wire:model="name" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề</label>
                                    <input class="form-control" wire:model="title" type="text">
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="1"
                                            wire:model="is_active">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Active
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" wire:model="is_active"
                                            id="flexRadioDefault2" value="0">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            Not Active
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Mô tả ngắn</label>
                                    <textarea class="form-control" wire:model="short_description" name="" id="" cols="30"
                                        rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="card">
                                <h1>Image</h1>
                                <div class="form-group">
                                    <label class="control-label">Hình ảnh</label>
                                    <input class="form-control" type="text" value="{{ $image }}">
                                    <a href="#" data-toggle="modal" data-target="#fileModal">(Bấm vào
                                        để
                                        chọn ảnh)</a>
                                </div>
                                <img src="{{ asset($image) }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <h1>Chi tiết</h1>
                                <div class="form-group">
                                    <label class="control-label">Mô tả</label>
                                    <x-input.tinymce wire:model="long_description" placeholder="Type anything you want..." />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    @include('livewire.admin.file.file-modal')
</div>
