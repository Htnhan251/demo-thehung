<div>
    <div class="card">
        <div class="card-header border-0">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="d-flex justify-content-between align-items-center">
                <h3 class="card-title">Danh sách sản phẩm</h3>
                <button class="btn btn btn-primary btn-sm bg-primary ">
                    <a href="{{ route('product.create') }}"> Thêm sản phẩm</a>
                </button>
            </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table list table-striped table-valign-middle">
                <thead>
                    <tr>
                        <th style="text-align: left">ID</th>
                        <th style="text-align: center">Sản phẩm</th>
                        <th style="text-align: center">Danh mục</th>
                        <th style="text-align: center">Giá</th>
                        <th style="text-align: center">Hình ảnh </th>
                        <th style="text-align: center">Hành động </th>
                    </tr>
                </thead>
                <tbody>
                    {{-- {{ dd($products) }} --}}
                    @foreach ($products as $item)
                        {{-- {{ dd($item->translate('image')) }} --}}
                        {{-- {{ dd($item->categories) }} --}}
                        <tr>
                            <td style="text-align: left">{{ $item->id }}</td>
                            <td style="text-align: center">{{ $item->translate('name') }}</td>
                            <td style="text-align: center">{{ $item->categories->translate('name')  }}</td>
                            <td style="text-align: center">{{ myCurrency($item->price) }} Vnđ</td>
                            <td style="text-align: center">
                                <img src="{{ asset($item->translate('image')) }}" width="150px" height="75px">
                            </td>
                            <td>
                                {{-- <button class="btn btn-sm btn-light " data-toggle="modal"
                                    data-target="#modalView-{{ $item->id }}">Xem nội dung code</button> --}}
                                <button class="btn btn-sm btn-info">
                                    <a href="{{ route('product.edit', $item->id) }}">Sửa</a>
                                </button>
                                <button class="btn btn-sm btn-light " wire:click="deleteComfirm({{ $item->id }})"
                                    data-toggle="modal" data-target="#deleteConfirmModel">Xóa</button>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
    {{ $products->links('livewire.paginate.custom-pagination-links') }}

    @include('livewire.admin.product.delete-modal')

</div>
