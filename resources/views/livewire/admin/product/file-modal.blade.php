<div class="modal fade modal-files" id="fileModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    tabindex="-1" wire:ignore.self>
    <div class="modal-dialog modal-confirm modal-lg">
        <div class="modal-content p-4">
            <div class="modal-header" style="padding: 5px 5px 5px 15px; border-bottom: none">
                {{-- <h4 class="modal-title">Chọn hình ảnh{{ $path }}</h4> --}}
            </div>
            {{-- <img src="{{ asset($path) }}" alt=""> --}}
            <div class="modal-body">
                <div class="d-flex px-2">
                    <input class="form-control me-2 mr-2" name='keyword' type="search" aria-label="Search"
                        wire:model="keyword" placeholder="Search">
                    <button class="btn btn-primary btn-sm" type="button" wire:click="searchFiles()">
                        Search
                    </button>
                </div>
                <div class="text-danger px-2"></div>

                <div id="spinner" wire:loading wire:target="search">
                    <i class="fa fa-spinner fa-spin"></i> Loading...
                </div>
                <div class="row">
                    @foreach ($files as $item)
                        <div class="col-6 col-sm-4 col-md-4 col-lg-4 col-xl-4" style="margin-top: 20px">
                            <div class="select-image-items">
                                <img src="{{ asset($item->path) }}" width="100%"
                                    wire:click="setImage({{ $item->id }},'{{ $item->path }}')" />
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" data-dismiss="modal" type="button">Cancel</button>
            </div>
        </div>
    </div>
</div>
