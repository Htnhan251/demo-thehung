<div>
    <div class="content">
        <div class="container-fluid">
            <div class="category__create-form">
                <form wire:ignore.self class="form-content" wire:submit.prevent action="">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="form-button">
                        <button class="btn btn btn-primary btn-sm bg-primary" wire:click="store()">Lưu</button>
                        <button class="btn btn-light btn-outline-dark  btn-sm">Exit</button>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="card">
                                <h1>Thông tin sản phẩm</h1>
                                {{-- <input type="hidden" wire:model="locale" value="{{ Config::get('app.locale') }}" /> --}}
                                <div class="form-group">
                                    <label class="control-label">Tên sản phẩm </label>
                                    <input class="form-control form-control-sm" wire:model="name" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề</label>
                                    <input class="form-control form-control-sm" wire:model="title" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">SKU</label>
                                    <input class="form-control form-control-sm" wire:model="sku" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Giá</label>
                                    <input class="form-control form-control-sm" wire:model="price" type="number">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Số lượng</label>
                                    <input class="form-control form-control-sm" wire:model="qty" type="number">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Giá khuyến mãi</label>
                                    <input class="form-control form-control-sm" wire:model="special_price"
                                        type="number">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ngày áp dụng khuyến mãi</label>
                                    <input class="form-control form-control-sm" wire:model="special_price_start"
                                        type="date">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Ngày kết thúc khuyến mãi</label>
                                    <input class="form-control form-control-sm" wire:model="special_price_end"
                                        type="date">
                                </div>


                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="card">
                                <div class="form-group">
                                    <label class="control-label">Danh mục</label>
                                    <select class="form-select" wire:model='category_id'>
                                        @foreach ($categories as $item)
                                            <option value="{{ $item->id }}">{{ $item->translate('name') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Mô tả ngắn</label>
                                    <textarea class="form-control" wire:model="short_description" cols="30" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tồn kho</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="1"
                                            wire:model="in_stock">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Active
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" wire:model="in_stock"
                                            id="flexRadioDefault2" value="0">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            Not Active
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Trạng thái</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" value="1"
                                            wire:model="is_active">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Active
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" wire:model="is_active"
                                            id="flexRadioDefault2" value="0">
                                        <label class="form-check-label" for="flexRadioDefault2">
                                            Not Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="card">
                                <h1>Image</h1>
                                <div class="form-group">
                                    <label class="control-label">Hình ảnh</label>
                                    <input class="form-control" type="text" value="{{ $image }}">
                                    <a href="#" data-toggle="modal" data-target="#fileModal">(Bấm vào
                                        để
                                        chọn ảnh)</a>
                                </div>
                                <img src="{{ asset($image) }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <h1>Chi tiết</h1>
                                <div class="form-group">
                                    <label class="control-label">Mô tả</label>
                                    <x-input.tinymce wire:model="long_description"
                                        placeholder="Type anything you want..." />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    @include('livewire.admin.file.file-modal')
</div>
