<div wire:ignore.self wire:submit.prevent class="modal fade" id="deleteConfirmModel" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel"  aria-hidden="true">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="material-icons">&#xE5CD;</i>
                    </div>
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" wire:click="delete()"
                        data-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>