<div>
    <div class="setting">
        <div class="card">
            <div class="container">
                <div class="setting__title ">
                    <div class="d-flex justify-content-between align-items-center">
                        <h1>Thông tin website</h1>
                        <button class="btn btn btn-primary btn-sm bg-primary " data-toggle="modal"
                            data-target="#modalCreate">Tạo thông tin web</button>
                    </div>
                </div>
                <hr>
                @if (session('success'))
                    <div class="alert alert-info alert-dismissable">
                        <i class="fa fa-coffee"></i>
                        {{ session('success') }}
                    </div>
                @endif
                <div class="setting__logo mb-3">
                    <div class="row">
                        <div class="col-md-3 setting_title--center">
                            <div class="setting__title">
                                <label>Logo</label>
                                <p>(Bấm vào hình logo để chọn ảnh khác )</p>
                            </div>
                        </div>
                        <div class="col-md-9 ">
                            <div class="setting__image">
                                <img {{-- src="{{ $logo == null ? asset(getInfoWeb('logo')) : asset($logo) }}" --}}
                                    src="{{ $logo == null && getInfoWeb('logo') == null ? asset('images/logo.jpg') : ($logo == null ? asset(getInfoWeb('logo')) : asset($logo)) }}"
                                    style="width: 25%" data-toggle="modal" wire:model="logo" data-target="#fileModal"
                                    alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="setting__info">
                    <form class="form-horizontal" role="form">
                        <div class="form-content">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 control-label">TIêu đề trang chủ</label>
                                    <div class="col-md-8" wire:model="title">
                                        <input class="form-control" type="text" value="{{ getInfoWeb('title') }}">
                                        {{-- @error('title')
                                            <span style="color: red">{{ $message }}</span>
                                        @enderror --}}
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 control-label">CSS</label>
                                    <div class="col-md-8" wire:model="css">
                                        <textarea class="form-control rounded" rows="3">{{ getInfoWeb('css') }}</textarea>
                                        {{-- @error('css')
                                            <span style="color: red">{{ $message }}</span>
                                        @enderror --}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-3 control-label">JS</label>
                                    <div class="col-md-8" wire:model="js">
                                        <textarea class="form-control rounded" rows="3">{{ getInfoWeb('js') }}</textarea>
                                        {{-- @error('css')
                                            <span style="color: red">{{ $message }}</span>
                                        @enderror --}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="setting__button">
                                    <input type="button" class="btn btn-primary" wire:click="update()"
                                        value="Lưu thông tin">
                                    <span></span>
                                    <input type="reset" class="btn btn-default" value="Hủy">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    {{--  --}}
    @include('livewire.admin.file.file-modal')

    @include('livewire.admin.web-info.add-info')
</div>
