<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    wire:submit.prevent="store()" wire:ignore.self data-backdrop="true" data-keyboard="false" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom-0">
                <h5 class="modal-title" id="exampleModalLabel">Tạo thông tin web</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                @if (session('success-store'))
                    <div class="alert alert-success">
                        {{ session('success-store') }}
                    </div>
                @endif
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tên thông tin web</label>
                        <input type="text" wire:model="name" class="form-control" placeholder="Mời nhập tên ">
                        @error('name')
                            <span style="color: red">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Nội dung thông tin web </label>
                        <input type="text" class="form-control" wire:model="value" placeholder="Mời nhập nội dung">
                        @error('value')
                            <span style="color: red">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer border-top-0 d-flex justify-content-center">
                    {{-- <input type="button" class="btn btn-primary bg-primary" wire:click="store()"
                            value="Add"> --}}
                    <button type="submit" class="btn btn-primary bg-primary">Thêm</button>
                </div>
            </form>
        </div>
    </div>
</div>
