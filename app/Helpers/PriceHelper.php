<?php
    function myCurrency($number){
        if(is_numeric($number)){
            return number_format($number,0,',','.');
        }
        return 'Please pass a number to this helper';
    }

?>