<?php

use App\Models\WebInformation;

function getLogo()
{
    $item =  WebInformation::where('name', 'logo')->first();

    return $item->value ?? null;
}


function getTitle()
{
    $item = WebInformation::where('name', 'title')->first();

    return $item->value ?? null;
}


function getInfoWeb($name)
{
    // dd(WebInformation::where('name',$name)->first());
    $item = WebInformation::where('name', $name)->first();
    return $item->value ?? null;
}


function getCSS()
{
    $data = WebInformation::where('name', 'css')->first();

    return $data->value ?? null;
}


function getJS()
{
    $data = WebInformation::where('name', 'js')->first();
    // dd($data->value);
    return $data->value ?? null;
}
