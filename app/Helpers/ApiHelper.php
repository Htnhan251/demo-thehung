<?php
function callAPI()
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://mgs-api-v2.internal.mangoads.com.vn/api/v1/posts?limit=100',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json' , 
        'Authorization: oEonkeqyD6Z1T1BwK8XaKHKCwBvVXZmLa',
        'Accept: application/json',
        'X-CHANNEL: sctv9',
        )
    ));

    $response = curl_exec($curl);
    //dd(curl_error($curl));
    curl_close($curl);

    return $response;
}
