<?php

use App\Models\Code;

function isType($type)
{
    return $type == 0 ? "<span class='bg-primary ' style='border-radius:20px;padding:0 10px' >CSS</span>"
        : "<span class='bg-warning ' style='border-radius:20px;padding:0 10px' >JS</span>";
}

function isStatus($status)
{
    return $status == 1 ? "<span class='bg-success ' style='border-radius:20px;padding:0 10px' >Đang kích hoạt</span>"
        : "<span class='bg-danger ' style='border-radius:20px;padding:0 10px' >Vô hiệu hóa</span>";
}


// function getCSS()
// {
//     $arrayCSS = [];
//     $data = Code::where('type', 0)->where('is_active', 1)->get();

//     foreach ($data as  $value) {
//         $arrayCSS[] = $value->content;
//     }
//     $arrayCSS = implode("\n", $arrayCSS);
//     // dd($arrayCSS);
//     return $arrayCSS;
// }

// function getJS()
// {
//     $arrayJS = [];

//     $data = Code::where('type', 1)->where('is_active', 1)->get();

//     foreach ($data as $value) {
//         $arrayJS[] = $value->content;
//     }
//     $arrayJS = implode("\n", $arrayJS);
//     // dd($arrayJS);
//     return $arrayJS;
// }


function isChecked($type)
{

    if($type == 0 )
    {
        return "checked";
    } 
    
    if($type == 1 )
    {
        return "checked";
    } 

}