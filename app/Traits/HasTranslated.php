<?php

namespace App\Traits;

use Illuminate\Support\Facades\Config;

trait HasTranslated
{

    /**
     * The "booting" method of the trait.
     *
     * @return void
     */
    protected static function bootHasTranslated()
    {
        static::saved(function ($entity) {
            $translatedModel = new $entity->translatedModel;

            $data =  request('serverMemo.data') ? request('serverMemo.data') : request()->all();

            $data['slug'] = createSlug($data['name']);
            $data['title'] = ($data['title'] != null) ? $data['title'] : $data['name'];

            foreach ($translatedModel->getFillable() as $field) {
                $translatedModel->$field = $data[$field];
            }

            if ($entity->translated()->exists()) {
                $entity->translated()->update(collect($data)->only($translatedModel->getFillable())->toArray());
            } else {
                $entity->translated()->save($translatedModel);
            }
        });
    }

    public function translated()
    {
        return $this->hasOne($this->translatedModel)->where('locale', '=', Config::get('app.locale'));
    }

    public function translate($key)
    {
        return $this->translated->$key != null ? $this->translated->$key :"null";
    }
}
