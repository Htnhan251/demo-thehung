<?php

namespace App\View\Composers;

use App\Models\Infoweb;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

class InformationComposer{
  
    public function compose(View $view)
    {
        $info = new Infoweb();
        $infoweb = $info::where('is_active',1)->first();
        
        $path = base_path('resources/views/backend/data/info_' . Config::get('app.locale') . '.json');
        $jsonString = file_get_contents($path);
        $data = json_decode($jsonString, true);
        $data = (object) $data;
        $view->with('information', $data)->with('info',$infoweb);
    }
}
