<?php

namespace App\Models;

use App\Traits\HasChannel;
use App\Traits\HasImage;
use App\Traits\HasTranslated;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasChannel, HasTranslated, HasImage;

    protected $translatedModel = ProductTranslation::class;
    
    protected $fillable = [
        'category_id',
        'price',
        'special_price',
        'special_price_start',
        'special_price_end',
        'selling_price',
        'sku',
        'qty',
        'in_stock',
        'viewed',
        'is_active',
        'new_form',
        'new_to'
    ];

    protected $with = ['categories', 'images'];

    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    
}
