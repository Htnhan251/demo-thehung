<?php

namespace App\Models;

use App\Traits\HasChannel;
use App\Traits\HasTag;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use HasFactory, SoftDeletes, HasChannel, HasTag;
    protected $fillable = [ 'name','company_name','short_name','phone','email','address','tax_no'];
}
