<?php

namespace App\Models;

use App\Traits\HasChannel;
use App\Traits\HasProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory, HasProduct, HasChannel;

    protected $fillable = ['name','sku','status', 'phone', 'address', 'note', 'tax', 'sub_total', 'total'];

    public function detail()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }
}
