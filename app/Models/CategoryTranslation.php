<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    use HasFactory;

    protected $fillable = ['locale', 'name', 'slug', 'title', 'short_description', 'long_description', 'image'];

    public function hasCategory()
    {
        return $this->belongsTo(Category::class);
    }
}
