<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SlideFile extends Model
{
    use HasFactory;

    protected $table = 'slide_file';
    protected $fillable = ['slide_id', 'file_id'];
}
