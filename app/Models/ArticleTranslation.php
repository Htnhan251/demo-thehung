<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleTranslation extends Model
{
    protected $fillable = [
        'name',
        'title',
        'locale',
        'slug',
        'image',
        'short_description',
        'long_description'
    ];
    use HasFactory;

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
