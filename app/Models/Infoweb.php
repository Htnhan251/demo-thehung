<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Infoweb extends Model
{
    use HasFactory;
    
    protected $fillable= ["name","address_1","address_2", "title_1","colum_1", "title_2", "colum_2", "title_3", "colum_3", "copyright", "is_active"];
    protected $table = "info_webs";


}
