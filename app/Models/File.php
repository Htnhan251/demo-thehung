<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'path', 'title', 'alt', 'note'];

    public function slides()
    {
        return $this->belongsToMany(Slide::class, 'slide_file', 'file_id', 'slide_id');
    }
}
