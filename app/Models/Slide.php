<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'content'];

    protected $with = ['files'];

    public function files()
    {
        return $this->belongsToMany(File::class, 'slide_file', 'slide_id', 'file_id');
    }
}
