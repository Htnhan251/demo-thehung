<?php

namespace App\Models;

use App\Traits\HasChannel;
use App\Traits\HasTranslated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Article extends Model
{
    use HasChannel, HasTranslated;

    protected $fillable = ['is_active','viewed'];

    protected $translatedModel = ArticleTranslation::class;

    protected $with = ['translated'];
}
