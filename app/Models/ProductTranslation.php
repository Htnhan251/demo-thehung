<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'locale',
        'slug',
        'long_description',
        'short_description',
        'image',
        'title',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
