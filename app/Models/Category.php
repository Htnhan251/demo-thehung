<?php

namespace App\Models;

use App\Traits\HasChannel;
use App\Traits\HasTranslated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Category extends Model
{
    use HasChannel, HasTranslated;

    protected $translatedModel = CategoryTranslation::class;

    protected $fillable = ['parent_id','position','is_active','type'];

    protected $with = ['translated'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    static function getListSelectCategory()
    {
        $cates = Category::where('is_active',1)->get();
        $categories = array();
        foreach ($cates as $key => $value) {
            $categories[$value->id] = $value->translated->name;
        }

        return $categories; // array id => name
    }
}
