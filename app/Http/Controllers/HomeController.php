<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Product;
use App\Models\Category;
use App\Models\Infoweb;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::where('is_active', 1)
        ->limit(0, 6)
        ->paginate(6);
        $homeArticle = Article::where('is_active', 1)
        ->take(6)
        ->get();

        // get slide
        $slides = array_diff(scandir(public_path('upload_data/banner')), array('.', '..'));

        return view('frontend.layouts.index', compact('products', 'homeArticle', 'slides'));
    }

    public function article()
    {
        $articaleList  = Article::where('is_active', 1)
        ->paginate(6);
        return view('frontend.blocks.article-list',compact('articaleList'));
    }

    public function category(string $slug)
    {
        $category = Category::whereHas('translated', function ($query) use ($slug) {
            $query->where('slug', '=', $slug);
        })
        ->with('products')
        ->get()
        ->first();

        $products = Product::where('is_active', 1)
        ->where('category_id', $category->id)
        ->paginate(18);

        return view('frontend.layouts.category', compact('products', 'category'));
    }

    public function productDetail(string $slug)
    {
        $categories = Category::where('is_active', 1)->get();
        $product = Product::whereHas('translated', function ($query) use ($slug) {
            $query->where('slug', '=', $slug);
        })->get()->first();
        $category = (object) collect($categories->toArray())->firstWhere('id', $product->category_id);
        $category->translated = (object) $category->translated;

        return view('frontend.layouts.product', compact('product', 'category'));
    }

    public function articleDetail(string $slug)
    {
        $article = Article::whereHas('translated', function ($query) use ($slug) {
            $query->where('slug', '=', $slug);
        })
        ->where('is_active', 1)
        ->get()
        ->first();

        return view('frontend.layouts.article', compact('article'));
    }

    public function contact()
    {       
        return view('frontend.layouts.contact');
    }

    public function storeContact(Request $request){

    }

    public function search(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $keyword = $data['keyword'];
        $slug = createSlug($keyword);
        //
        $products = Product::whereHas('translated', function ($query) use ($slug) {
            $query->where('slug', 'LIKE', '%' . $slug . '%');
        })->paginate(15);

        $articles = Article::whereHas('translated', function ($query) use ($slug) {
            $query->where('slug', 'LIKE', '%' . $slug . '%');
        })->paginate(15);

        
        return view('frontend.layouts.search',compact('products','articles','keyword'));
        // if (strlen($keyword) >= 4) {
        //     $products = Product::whereHas('translated', function ($query) use ($slug) {
        //         $query->where('slug', 'LIKE', '%' . $slug . '%');
        //     })->paginate(15);

        //     $articles = Article::whereHas('translated', function ($query) use ($slug) {
        //         $query->where('slug', 'LIKE', '%' . $slug . '%');
        //     })->paginate(15);

            
        //     return view('frontend.layouts.search',compact('products','articles','keyword'));
        // }

        // return view('frontend.layouts.search')->with('error', 'Keyword must be at least 5 characters!');
    }      

}
