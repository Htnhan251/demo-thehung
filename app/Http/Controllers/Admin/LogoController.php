<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Logo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function changeLogoImage(Request $request)
    {
        // dd($request->file('logo-image'));
        if ($request->has('logo-image')) {
            $file = $request->file('logo-image');
            $fileName = $file->getClientOriginalName();

            if (!File::isDirectory(public_path('images/logo'))) {
                File::makeDirectory(public_path('images/logo'));

                $file->move(public_path('images/logo'), $fileName);
                config()->set('logo-image.logo' , asset("images/logo/".$fileName));
                Logo::create([
                    'name'=>$fileName
                ]);
            } else {
                if (File::isDirectory(public_path('images/logo'))) {
                    $file->move(public_path('images/logo'), $fileName);
                    Logo::create([
                        'name'=>$fileName
                    ]);
                }
            }
        }

        return redirect()->route('admin.dashboard');
    }
}
