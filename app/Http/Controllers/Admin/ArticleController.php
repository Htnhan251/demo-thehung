<?php

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends BaseController
{

    public function __construct()
    {
        $this->model = new Article();
        parent::__construct();        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Article $article)
    {
        $data = $request->all();
        $new = Article::create(collect($data)->only($article->getFillable())->toArray());

        return redirect()->route('article.edit', $new->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $data = $request->all();
        Article::find($article->id)->update(collect($data)->only($article->getFillable())->toArray());

        return redirect()->route('article.edit', $article->id);
    }
}
