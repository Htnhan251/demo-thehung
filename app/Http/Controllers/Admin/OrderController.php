<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    public function __construct()
    {
        $this->model = new Order();
        parent::__construct();
    }

    public function store(Request $request)
    {
        return $this->insert_data($request);
    }


    public function update(Request $request, Order $order)
    {
        return $this->insert_data($request, $order);
    }
}
