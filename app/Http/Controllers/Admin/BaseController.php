<?php

namespace App\Http\Controllers\Admin;

use App\Models\Channel;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public $model;

    public $modelName;

    public $items_per_page = 10;

    public $sort_by = 'id';

    public $order_by = 'DESC';

    public function __construct()
    {
        if ($this->model) {
            $this->modelName = class_basename($this->model);
        }
    }

    public function index()
    {
        $channel = session()->get('channel');

        $q = $this->model->orderBy($this->sort_by, $this->order_by);
        if ($channel) {
            $q->whereHas('channels', function ($query) use ($channel) {
                $query->where('slug', $channel->slug);
            });
        }
        $data = $q->paginate($this->items_per_page);        

        return view('backend.layouts.' . strtolower($this->modelName) . '.index', compact('data'));
    }

    public function insert_data($request, $model = null)
    {
        $data = $request->all();
        $new = null;
        if ($model) {
            $this->model::find($model->id)
            ->update(
                collect($data)
                ->only($model->getFillable())
                ->toArray()
            );
        } else {
            $new = $this->model::create(
                collect($data)
                ->only($this->model->getFillable())
                ->toArray()
            );
        }

        return redirect()->route(strtolower($this->modelName) . '.edit', $new ? $new->id : $model->id);
    }

    public function create()
    {
        $data = $this->model;

        return view('backend.layouts.' . strtolower($this->modelName) . '.create', compact('data'));
    }

    public function edit(int $id)
    {
        $data = $this->model::find($id);

        return view('backend.layouts.' . strtolower($this->modelName) . '.edit', compact('data'));
    }

    public function destroy(int $id)
    {
        $result = $this->model::destroy($id);

        return $result ? 'deleted' : 'error';
    }

    public function dashboard()
    {
        return view('backend.main');
    }

    public function changeLanguage(string $lang)
    {
        session()->put('lang', $lang);

        return redirect()->back();
    }

    public function channel(string $channelSlug)
    {
        $channel = Channel::where('slug', $channelSlug)->first();
        if ($channel) {
            session()->put('channel', $channel);
        } else {
            session()->forget('channel');
        }

        return redirect()->back();
    }
}
