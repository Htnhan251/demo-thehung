<?php

namespace App\Http\Controllers\Admin;

use App\Models\Channel;
use Illuminate\Http\Request;

class ChannelController extends BaseController
{
    public function __construct()
    {
        $this->model = new Channel();
        parent::__construct();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token','_method');
        $data['slug'] = createSlug($data['name']);
        $channels = Channel::create($data);

        return redirect()->route('channels.edit',$channels->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Channel $channel)
    {
        $data = $request->except('_token','_method');
        $data['slug'] = createSlug($data['name']);
        if ($data['is_default'] == 1) {
            Channel::whereNotIn('id',[$channel->id])->update(['is_default' => false]);
        }

        Channel::find($channel->id)->update($data);

        return redirect()->back();
    }


}
