<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Infoweb;
use Illuminate\Http\Request;

class InfoWebController extends BaseController
{
    public function __construct()
    {
        $this->model = new Infoweb();
        parent::__construct() ;
        
    }

    public function store(Request $request){
        if($request->is_active==1){

            // $data = $this->model::where('is_active',1)->first();
            // if($data){                  
            //     $data->is_active = '0';                 
            //     $this->model::find($data->id)->update($data->toArray());
            // } 

            $this->model::where('is_active',1)->update(['is_active' => 0]);

        }
        return $this->insert_data($request);      
          
    }

    public function update(Request $request, Infoweb $infoweb){
        if($request->is_active==1){

            // $data_change = $this->model::where('is_active',1)->first();
            // if($data_change){                  
            //     $data_change->is_active = '0';                 
            //     $this->model::find($data_change->id)->update($data_change->toArray());
            // }    
            
            $this->model::where('is_active',1)->update(['is_active' => 0]);
        }
        $data = $request->all();

        Infoweb::find($infoweb->id)->update(collect($data)->only($infoweb->getFillable())->toArray());

        return redirect()->route('infoweb.edit',$infoweb->id);      
    }
}
