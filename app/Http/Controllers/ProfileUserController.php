<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserInfoRequest;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileUserController extends Controller
{

    public function index()
    {
        if(Auth::user()):
            return view('frontend.layouts.profile.info');
        endif;      
        
        return redirect()->back();
        
    }

    public function update(UserInfoRequest $request, int $id)
    {
        User::find($id)->update($request->all());            
    }
}
