<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\CategoryCollection;
use App\Http\Resources\API\CategoryDetailResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $items = 10; // items per page

    public function index()
    {
        $data = Category::with('translated')
        ->where('is_active',1)
        ->paginate($this->items);
        
        return new CategoryCollection($data);
    }

    public function slug(string $slug)
    {
        $data = Category::with('translated')
        ->whereHas('translated',function ($query) use($slug) {
            $query->where('slug',$slug);
        })
        ->where('is_active',1)
        ->first();

        return new CategoryDetailResource($data);
    }

    public function show(int $id)
    {
        $data = Category::with('translated')
        ->where('is_active',1)
        ->where('id',$id)
        ->first();

        return new CategoryDetailResource($data);
    }
}
