<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\ProductCollection;
use App\Http\Resources\API\ProductDetailResource;
use App\Models\Product;

class ProductController extends Controller
{
    protected $items = 10; // items per page

    public function index()
    {
        $data = Product::with('translated', 'categories')
        ->where('is_active',1)
        ->paginate($this->items);

        return new ProductCollection($data);
    }

    public function slug(string $slug)
    {
        $data = Product::with('translated','categories')
        ->whereHas('translated',function ($query) use($slug) {
            $query->where('slug',$slug);
        })
        ->where('is_active',1)
        ->first();

        return new ProductDetailResource($data);
    }

    public function show(int $id)
    {
        $data = Product::with('translated','categories')
        ->where('is_active',1)
        ->where('id',$id)
        ->first();

        return new ProductDetailResource($data);
    }
}
