<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\TagCollection;
use App\Http\Resources\API\TagDetailResource;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    protected $items = 10; // items per page

    public function index(){
        $data = Tag::where('is_active',1)->paginate($this->items);
        // dd($data);

        return new TagCollection($data);
    }

    public function slug(string $slug){
        // dd($slug);
        $data = Tag::where('is_active',1)->where('slug',$slug)->first();
        // dd($data);
        return new TagDetailResource($data);
       
    }
}
