<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\ArticleCollection;
use App\Http\Resources\API\ArticleDetailResource;
use App\Models\Article;

class ArticleController extends Controller
{
    protected $items = 10; // items per page

    public function index()
    {
        $data = Article::with('translated')
        ->where('is_active',1)
        ->paginate($this->items);

        return new ArticleCollection($data);
    }

    public function slug(string $slug)
    {
        $data = Article::with('translated','categories')
        ->whereHas('translated',function ($query) use($slug) {
            $query->where('slug',$slug);
        })
        ->where('is_active',1)
        ->first();

        return new ArticleDetailResource($data);
    }

    public function show(int $id)
    {
        $data = Article::with('translated','categories')
        ->where('is_active',1)
        ->where('id',$id)
        ->first();

        return new ArticleDetailResource($data);
    }

}
