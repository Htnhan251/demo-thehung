<?php

namespace App\Http\Resources\API;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"=> $this->id,
            "category"=> $this->categories->translated->name,
            "price"=> $this->price,
            "sku"=> $this->sku,
            "in_stock"=> $this->in_stock,
            "is_active"=> $this->is_active,
            "name"=> $this->translated->name,
            "locale"=> $this->translated->locale,
            "slug"=> $this->translated->slug,
            "short_description"=> $this->translated->short_description,
            "image"=> $this->translated->image,
            "title"=> $this->translated->title,
            "created_at"=> Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('Y-m-d'),
            "updated_at"=> Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at)->format('Y-m-d'),
        ];
    }
}
