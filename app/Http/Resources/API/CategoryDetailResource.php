<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->translated->name,
            'locale' => $this->translated->locale,
            'image' => $this->translated->image,
            'slug' => $this->translated->slug,
            'title' => $this->translated->title,
            'short_description' => $this->translated->short_description,
            'long_description' => $this->translated->long_description,
            'parent_id' => $this->parent_id,
            'position' => $this->position,
            'is_active' => $this->is_active,
            'type' => $this->type,
        ];
    }
}
