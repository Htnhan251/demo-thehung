<?php

namespace App\Http\Livewire\Front;

use App\Models\Product;
use App\Models\ProductTranslation;
use Livewire\Component;

class Search extends Component
{
    public $search;
    public function render()
    {
    $search = $this->search;
    $noti = null;
    $products = [];

    if (strlen($search) >= 3) {
        $products = Product::whereHas('translated', function ($query) use ($search) {
            $query->where('name', 'like', '%' . $search . '%');
        })->take(5)->get();

        if ($products->count() == 0) {
            $noti = 'Khong co ket qua';
        }
    }

    if (0 < strlen($search) && strlen($search) < 3) {
        $noti = 'Vui long nhap nhieu hon 3 ky tu';
    }

    return view('livewire.front.search', compact('products','noti'));
}
}
