<?php

namespace App\Http\Livewire\Admin\WebInfo;

use App\Models\File;
use App\Models\WebInformation;
use Livewire\Component;
use Livewire\WithPagination;

class WebInfoIndex extends Component
{
    public $logo, $title, $css, $js, $name, $value;
    // $model

    // public function mount()
    // {
    //     $this->model = new WebInformation();
    // }

    use WithPagination;
    
    protected $rules = [
        'name' => 'required|unique:web_information',
        'value' => 'required',
        // 'js' => 'required',
    ];

    public function render()
    {
        // dd($this->model);
        $files = File::paginate(8);
        return view('livewire.admin.web-info.web-info-index', compact('files'))->extends('backend.main')->section('content');
    }


    public function updatedName($name)
    {
        if ($name === 'logo') {
            $this->value = 'images/logo.jpg';
        }
    }

    public function update()
    {
        // $this->validate();

        // dd(request('serverMemo.data'));
        $infos = WebInformation::all();
        $data =  request('serverMemo.data');
        foreach ($infos as $info) {
            foreach ($data as $key => $value) {
                // dd($key);
                if ($info->name == $key && $value != null) {
                    $info->value = $value;
                    // dd($value);
                    $info->save();
                }
                // dd('dasd');

            }
        }

        session()->flash('success', 'Updated Successfully');
    }

    public function store()
    {

        $this->validate();
        // dd(collect(request('serverMemo.data'))->only('name','value')->toArray());
        $data = collect(request('serverMemo.data'))->only('name', 'value')->toArray();

        WebInformation::create($data);

        session()->flash('success-store', 'Tạo thông tin thành công');
    }


    public function setImage($id, $path)
    {
        // dd($this->model->value);
        $this->logo = $path;
        // dd($this->model->value);
    }
}
