<?php

namespace App\Http\Livewire\Admin\Category;

use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;


class CategoryIndex extends Component
{
    public $locale, $cateId;

    use WithPagination;

    public function render()
    {
        $category = Category::paginate(10);

        return view('livewire.admin.category.category-index', compact('category'))->extends('backend.main')->section('content');
    }

    public function setLocale()
    {
        $this->locale = 'vi';
        $this->emit('localeChanged', $this->locale);
    }

    public function deleteComfirm($id)
    {
        $this->cateId = $id;
    }

    public function delete()
    {
        $cate = Category::find($this->cateId);

        $cate->delete();

        session()->flash('success','Xóa thành công');
    }
}
