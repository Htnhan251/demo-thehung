<?php

namespace App\Http\Livewire\Admin\Category;

use App\Models\Category;
use App\Models\File;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use Livewire\WithPagination;

class CategoryEdit extends Component
{
    public $cateId, $name, $title, $slug, $is_active, $short_description, $long_description, $image, $locale;

    use WithPagination;

    protected $rules = [
        'name' => 'required',
        'title' => 'required',
        'is_active' => 'required',
        'short_description' => 'required',
        'long_description' => 'required',

    ];


    public function mount(Category $category)
    {
        $this->cateId = $category->id;
        $this->setData($category);
    }

    public function render()
    {
        $files = File::paginate(8);
        return view('livewire.admin.category.category-edit', compact('files'))->extends('backend.main')->section('content');
    }

    public function setData($model)
    {
        $this->name = $model->translate('name');
        $this->title = $model->translate('title');
        $this->slug = $model->translate('slug');
        $this->is_active = $model->is_active;
        $this->short_description = $model->translate('short_description');
        $this->long_description = $model->translate('long_description');
        $this->image = $model->translate('image');
        $this->locale = Config::get('app.locale');
    }


    public function update()
    {
        $this->validate();
        // dd(request('serverMemo.data'));
        $data = request('serverMemo.data');

        $cate = Category::find($this->cateId);

        $cate->update($data);

        session()->flash('success', "Cập nhật thành công");
    }

    public function setImage($id, $path)
    {
        // dd($path);
        $this->image = $path;
    }
}
