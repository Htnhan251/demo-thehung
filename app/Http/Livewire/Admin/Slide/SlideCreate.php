<?php

namespace App\Http\Livewire\Admin\Slide;

use App\Models\File;
use App\Models\Slide;
use App\Models\SlideFile;
use Livewire\Component;
use Livewire\WithPagination;

class SlideCreate extends Component
{

    public $path, $title, $listImages = [];

    use WithPagination;

    protected $rules = [
        'title' => 'required',
    ];


    public function render()
    {
        $files = File::paginate(8);
        $slides = Slide::paginate(8);

        return view('livewire.admin.slide.slide-create', compact('files', 'slides'))->extends('backend.main')->section('content');
    }

    public function setImage($id, $path)
    {
        // dd($this->model->value);
        $this->path = $path;
        // dd($this->model->value);
        $this->listImages[$id] = $this->path;
    }



    public function RemoveImageToSlide($id)
    {
        // dd($this->arr);

        foreach ($this->listImages as $key => $value) {
            if ($id == $key) {
                unset($this->listImages[$id]);
            }
        }

        // dd($this->arr);
    }

    public function store()
    {
        $this->validate();

        $data = request('serverMemo.data');
        // dd($data);
        $content = "";
        // dd((string) $this->arr);
        foreach ($this->listImages as $key => $value) {
            $content .= $value . " ";
        }

        $slide = Slide::create([
            "title" => $data['title'],
            'content' => $content
        ]);

        $slide->save();

        $slideId = $slide->id;

        foreach ($this->listImages as $key => $value) {
            $pivot = SlideFile::create([
                'slide_id' => $slideId,
                'file_id' => $key
            ]);
            $pivot->save();
        }


        session()->flash('success', 'Tạo slide thành công');

        return redirect()->route('slide.edit', $slideId);
    }
}
