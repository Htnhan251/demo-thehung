<?php

namespace App\Http\Livewire\Admin\Slide;

use App\Models\File;
use App\Models\Slide;
use App\Models\SlideFile;
use Livewire\Component;
use Livewire\WithPagination;

class SlideEdit extends Component
{

    public $slideID, $model, $title, $path, $listImages = [], $listRemoveImage = [], $listAddImage = [];

    use WithPagination;

    public function mount(Slide $slide)
    {
        // dd($slide->id);
        $this->model = $slide;
        $this->title = $slide->title;
        // dd($this->model->files);

        foreach ($this->model->files as $key => $value) {
            $this->listImages[$value->id] = $value->path;
        }
        // dd($this->listImages);
    }

    public function render()
    {

        $itemSlide = $this->model;
        $files = File::paginate(8);
        // dd($itemSlide->files->toarray());
        return view('livewire.admin.slide.slide-edit', compact('itemSlide', 'files'))->extends('backend.main')->section('content');
    }


    public function setImage($id)
    {
        $file = File::select('id')->find($id);
        $this->listAddImage[] = $file->id;
        // dd($this->listAddImage);
    }

    public function RemoveImageToSlide($id)
    {

        // $listRemove = [];

        $file = File::select('id')->find($id);

        $this->listRemoveImage[] = $file->id;

        // dd($this->listRemoveImage);

        // $data = File::select('path')->find($id);

        foreach ($this->listImages as $key => $value) {

            if ($key == $id) {
                unset($this->listImages[$id]);
            }
        }
    }




    public function update()
    {
        $data = collect(request('serverMemo.data'))->only('title', 'listRemoveImage', 'listAddImage')->toArray();
        $this->model->update([
            'title' => $data['title'],
        ]);

        if (count($this->listRemoveImage) > 0) {
            foreach ($this->listRemoveImage as $k => $v) {
                // dd($v);
                SlideFile::where('slide_id', $this->model->id)
                    ->where('file_id', $v)
                    ->delete();
            }
        }

        if (count($this->listAddImage) > 0) {
            foreach ($this->listAddImage as $k => $v) {
                SlideFile::create([
                    'slide_id' => $this->model->id,
                    'file_id' => $v
                ]);
            }
        }
        $this->redirect(route('slide.edit', $this->model->id));
    }
}
