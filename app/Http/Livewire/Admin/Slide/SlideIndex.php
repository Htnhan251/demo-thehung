<?php

namespace App\Http\Livewire\Admin\Slide;

use App\Models\File;
use App\Models\Slide;
use App\Models\SlideFile;
use Livewire\Component;
use Livewire\WithPagination;

class SlideIndex extends Component
{
    public $path, $slideId, $title, $arr = [];

    use WithPagination;

    public function render()
    {
        $files  = File::paginate(8);
        $slides = Slide::paginate(8);
        return view('livewire.admin.slide.slide-index', compact('files', 'slides'))->extends('backend.main')->section('content');
    }


    public function setImage($id, $path)
    {
        // dd($this->model->value);
        $this->path = $path;
        // dd($this->model->value);
        $this->arr[$id] = $this->path;
    }



    public function RemoveImageToSlide($id)
    {
        // dd($this->arr);

        foreach ($this->arr as $key => $value) {
            if ($id == $key) {
                unset($this->arr[$id]);
            }
        }

        // dd($this->arr);
    }

    public function store()
    {

        $data = request('serverMemo.data');
        // dd($data['title']);
        $content = "";
        // dd((string) $this->arr);
        foreach ($this->arr as $key => $value) {
            $content .= $value . " ";
        }

        $slide = Slide::create([
            "title" => $data['title'],
            'content' => $content
        ]);

        $slide->save();

        $slideId = $slide->id;

        foreach ($this->arr as $key => $value) {
            $pivot = SlideFile::create([
                'slide_id' => $slideId,
                'file_id' => $key
            ]);
            $pivot->save();
        }
        // dd($slide->id);
        // dd($data);
    }



    public function deleteComfirm($id)
    {
        $this->slideId = $id;
    }

    public function delete()
    {
        $item  = Slide::find($this->slideId);

        $item->delete();

        session()->flash('success', 'Đã xóa thành công');
    }
}
