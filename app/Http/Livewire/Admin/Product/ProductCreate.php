<?php

namespace App\Http\Livewire\Admin\Product;

use App\Models\Category;
use App\Models\File;
use App\Models\Product;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class ProductCreate extends Component
{

    // public   $model, $image;
    public $title, $sku, $name, $long_description, $short_description,
        $in_stock, $is_active, $special_price_start, $special_price_end,
        $image, $category_id, $price, $special_price, $qty, $locale;

    protected $rules = [
        'name' => 'required',
        'title' => 'required',
        'is_active' => 'required',
        'short_description' => 'required',
        'long_description' => 'required',
        'in_stock' => 'required',
        'price' => 'required',
        'category_id' => 'required',
        'qty' => 'required',
        'sku' => 'required',
        'image' => 'nullable',
        'special_price_start' => 'nullable',
        'special_price_end' => 'nullable',
        'special_price' => 'nullable',
        // 'locale' => 'required'
    ];

    public function mount()
    {
        // $this->model = new Product();
        $this->locale =  Config::get('app.locale');
    }

    public function render()
    {
        $categories = Category::all();
        $files = File::paginate(8);
        return view('livewire.admin.product.product-create', compact('categories', 'files'))->extends('backend.main')->section('content');
    }


    public function store()
    {

        // dd(request('serverMemo.data'));
        $this->validate();
        // dd(request('serverMemo.data'));

        $data = request('serverMemo.data');

        $product  = Product::create($data);

        session()->flash('success', 'Tạo sản phẩm thành công');

        return redirect()->route('product.edit', $product->id);
    }


    public function setImage($id, $path)
    {
        $this->image = $path;
    }
}
