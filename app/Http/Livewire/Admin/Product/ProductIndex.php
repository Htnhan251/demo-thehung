<?php

namespace App\Http\Livewire\Admin\Product;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithPagination;

class ProductIndex extends Component
{
    use WithPagination;

    public $productId;

    public function render()
    {
        $products = Product::paginate(10);
        return view('livewire.admin.product.product-index', compact('products'))->extends('backend.main')->section('content');
    }



    public function deleteComfirm($id)
    {
        $this->productId = $id;
    }

    public function delete()
    {

        $product = Product::find($this->productId);

        $product->delete();

        session()->flash('success', 'Đã xóa thành công');
    }
}
