<?php

namespace App\Http\Livewire\Admin\Product;

use App\Models\File;
use App\Models\Product;
use Illuminate\Support\Facades\Config;
use Livewire\Component;

class ProductEdit extends Component
{
    public $model, $productId, $title, $sku, $name, $long_description, $short_description,
        $in_stock, $is_active, $special_price_start, $special_price_end,
        $image, $category_id, $price, $special_price, $qty, $locale;

    public function mount(Product $product)
    {
        $this->model = $product;
        $this->productId = $product->id;
        $this->locale =  Config::get('app.locale');
        $this->setData($this->model);
    }


    public function setData($model)
    {
        $this->title = $model->translate('title');
        $this->name = $model->translate('name');
        $this->sku = $model->sku;
        $this->price = $model->price;
        $this->long_description = $model->translate('long_description');
        $this->short_description = $model->translate('short_description');
        $this->in_stock = $model->in_stock;
        $this->is_active = $model->is_active;
        $this->special_price_start = $model->special_price_start;
        $this->special_price_end = $model->special_price_end;
        $this->image = $model->translate('image');
        $this->qty = $model->qty;
        $this->special_price = $model->special_price;
        $this->category_id = $model->category_id;
    }

    public function update()
    {
        // dd(collect(request('serverMemo.data'))->except('model')->toArray());
        $data  = collect(request('serverMemo.data'))->except('model')->toArray();

        $item =  Product::find($this->productId);

        $item->update($data);

        session()->flash('success', 'Cập nhật thành công');
    }


    public function render()
    {
        $files = File::paginate(8);
        return view('livewire.admin.product.product-edit', compact('files'))->extends('backend.main')->section('content');
    }


    public function setImage($id, $path)
    {
        $this->image = $path;
    }
}
