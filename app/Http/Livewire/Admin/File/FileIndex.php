<?php

namespace App\Http\Livewire\Admin\File;

use App\Models\File;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;



class FileIndex extends Component
{
    use WithFileUploads, WithPagination;

    public $photo, $fileId, $alt, $note, $title, $mess;


    public function render()
    {
        $data = File::paginate(8);
        // dd($data);
        return view('livewire.admin.file.file-index', compact('data'))->extends('backend.main')->section('content');
    }


    public function save()
    {
        $this->validate([
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048', // 1MB Max
        ]);

        $this->photo->storeas('public/images', $this->photo->getClientOriginalName());
        File::create([
            'name' => $this->photo->getClientOriginalName(),
            'path' => 'storage/images/' . $this->photo->getClientOriginalName()
        ]);
        $this->photo = null;
        session()->flash('success', "Thêm thành công");
    }

    public function edit($id)
    {
        $this->fileId = $id;

        $item = File::find($id);

        $this->alt = $item->alt ?? null;

        $this->note = $item->note ?? null;

        $this->title = $item->title ?? null;
    }

    public function update()
    {
        $this->validate([
            'alt' => 'required',
            'note' => 'required',
            'title' => 'required'
        ]);

        $data = collect(request('serverMemo.data'))->only(['alt', 'title', 'note'])->toArray();
        $item = File::find($this->fileId);

        $item->update($data);

        session()->flash('success', 'Cập nhật thành công');
    }

    public function deleteConfirm($id)
    {
        $this->fileId = $id;
    }

    public function delete()
    {
        $item = File::find($this->fileId);

        $item->delete();
    }
}
