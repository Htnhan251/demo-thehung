<?php

namespace App\Http\Livewire\Admin\Article;

use App\Models\Article;
use App\Models\File;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use Livewire\WithPagination;


class ArticleEdit extends Component
{
    public $articleID, $name, $title, $slug, $is_active, $short_description, $long_description, $image, $locale;


    use WithPagination;

    protected $rules = [
        'name' => 'required',
        'title' => 'required',
        'is_active' => 'required',
        'short_description' => 'required',
        'long_description' => 'required',

    ];

    public function mount(Article $article)
    {
        $this->articleID = $article->id;
        $this->setData($article);
    }


    public function setData($model)
    {
        $this->name = $model->translate('name');
        $this->title = $model->translate('title');
        $this->slug = $model->translate('slug');
        $this->is_active = $model->is_active;
        $this->short_description = $model->translate('short_description');
        $this->long_description = $model->translate('long_description');
        $this->image = $model->translate('image');
        $this->locale = Config::get('app.locale');
    }

    public function setImage($id, $path)
    {
        $this->image = $path;
    }

    public function render()
    {
        $files = File::paginate(8);
        return view('livewire.admin.article.article-edit', compact('files'))->extends('backend.main')->section('content');
    }


    public function update()
    {
        $this->validate();
        // dd(request('serverMemo.data'));
        $data = request('serverMemo.data');

        $article = Article::find($this->articleID);

        $article->update($data);

        session()->flash('success', "Cập nhật thành công");
    }
}
