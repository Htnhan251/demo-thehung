<?php

namespace App\Http\Livewire\Admin\Article;

use App\Models\Article;
use Livewire\Component;
use Livewire\WithPagination;


class ArticleIndex extends Component
{

    public $articleId;

    use WithPagination;

    public function render()
    {

        $articles = Article::paginate(10);

        return view('livewire.admin.article.article-index', compact('articles'))->extends('backend.main')->section('content');
    }


    public function deleteComfirm($id)
    {
        $this->articleId = $id;
    }

    public function delete()
    {
        $cate = Article::find($this->articleId);

        $cate->delete();

        session()->flash('success','Xóa thành công');
    }
}
