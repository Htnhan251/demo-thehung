<?php

namespace App\Http\Livewire\Admin\Article;

use App\Models\Article;
use App\Models\File;
use Illuminate\Support\Facades\Config;
use Livewire\Component;
use Livewire\WithPagination;

class ArticleCreate extends Component
{
    public $name, $title, $slug, $is_active, $short_description, $long_description, $image, $locale;

    use WithPagination;
    
    protected $rules = [
        'name' => 'required',
        'title' => 'required',
        'is_active' => 'required',
        'short_description' => 'required',
        'long_description' => 'required',

    ];

    public function mount()
    {
        $this->locale =  Config::get('app.locale');
    }

    public function render()
    {
        $files = File::paginate(8);
        return view('livewire.admin.article.article-create', compact('files'))->extends('backend.main')->section('content');
    }

    public function setImage($id, $path)
    {
        $this->image = $path;
    }

    public function store()
    {
        $this->validate();

        $data =  request('serverMemo.data');
        // dd($data);
        $article = Article::create($data);

        session()->flash('success', 'Tạo danh mục thành công');
        return redirect()->route('article.edit', $article->id);
    }
}
