<?php

namespace App\Http\Livewire\Admin\Code;

use App\Models\Code;
use Livewire\Component;

class CodeIndex extends Component
{
    public $title, $content, $is_active, $type, $codeId;
    public function render()
    {
        $data = Code::paginate(10);
        return view('livewire.admin.code.code-index', compact('data'))->extends('backend.main')->section('content');
    }


    public function resetData()
    {
        $this->title = null;
        $this->content = null;
        $this->is_active = null;
        $this->type = null;
    }

    public function store()
    {
        // dd(request('serverMemo.data'));

        $data = request('serverMemo.data');

        Code::create($data);
    }


    public function update()
    {

        $data = request('serverMemo.data');
        // dd($data);
        $item = Code::find($this->codeId);

        $item->update($data);
    }


    public function view($id)
    {
        $item = Code::find($id);

        $this->content = $item->content ?? null;
    }

    public function edit($id)
    {
        $this->codeId = $id;
        $item = Code::find($id);

        $this->title = $item->title ?? null;
        $this->content = $item->content ?? null;
        $this->type = $item->type ?? null;
        $this->is_active = $item->is_active ?? null;
    }

    public function deleteComfirm($id)
    {
        $this->codeId = $id;
    }

    public function delete()
    {
        $item = Code::find($this->codeId);

        $item->delete();
    }
}
