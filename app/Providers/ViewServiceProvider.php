<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\View\Composers\CategoryComposer;
use App\View\Composers\ChannelComposer;
use App\View\Composers\InformationComposer;
use App\View\Composers\ProductComposer;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer('backend.layouts.order._form', ProductComposer::class);
        View::composer('*', CategoryComposer::class);
        View::composer('frontend.*', InformationComposer::class);
        View::composer('backend.*',ChannelComposer::class);
    }
}
