<?php

use App\Http\Controllers\API\ArticleController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\Api\TagController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('products')->group(function(){
    Route::get('/',[ProductController::class, 'index'])->name('products.index');
    Route::get('/{id}',[ProductController::class, 'show'])->name('products.show');
    Route::get('slug/{slug}',[ProductController::class, 'slug'])->name('products.slug');
});

Route::prefix('articles')->group(function(){
    Route::get('/',[ArticleController::class, 'index'])->name('articles.index');
    Route::get('/{id}',[ArticleController::class, 'show'])->name('articles.show');
    Route::get('slug/{slug}',[ArticleController::class, 'slug'])->name('articles.slug');
});

Route::prefix('categories')->group(function(){
    Route::get('/',[CategoryController::class, 'index'])->name('categories.index');
    Route::get('/{id}',[CategoryController::class, 'show'])->name('categories.show');
    Route::get('slug/{slug}',[CategoryController::class, 'slug'])->name('categories.slug');
});

Route::prefix('orders')->group(function(){
    Route::get('/{id}',[OrderController::class, 'show'])->name('order.show');
    Route::post('/',[OrderController::class, 'store'])->name('order.store');
});

Route::prefix('tags')->group(function(){
    Route::get('/',[TagController::class,'index'])->name('tag.index');
    Route::get('slug/{slug}',[TagController::class,'slug'])->name('tag.slug');
});
