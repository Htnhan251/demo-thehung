<?php

use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\InformationController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ChannelController;
use App\Http\Controllers\Admin\InfoWebController;
use App\Http\Controllers\Admin\LogoController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PartnerController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Livewire\Admin\Article\ArticleCreate;
use App\Http\Livewire\Admin\Article\ArticleEdit;
use App\Http\Livewire\Admin\Article\ArticleIndex;
use App\Http\Livewire\Admin\Category\CategoryCreate;
use App\Http\Livewire\Admin\Category\CategoryEdit;
use App\Http\Livewire\Admin\Category\CategoryIndex;
use App\Http\Livewire\Admin\Code\CodeIndex;
use App\Http\Livewire\Admin\File\FileIndex;
use App\Http\Livewire\Admin\Product\ProductCreate;
use App\Http\Livewire\Admin\Product\ProductEdit;
use App\Http\Livewire\Admin\Product\ProductIndex;
use App\Http\Livewire\Admin\Slide\LogoIndex;
use App\Http\Livewire\Admin\Slide\SlideCreate;
use App\Http\Livewire\Admin\Slide\SlideEdit;
use App\Http\Livewire\Admin\Slide\SlideIndex;
use App\Http\Livewire\Admin\WebInfo\WebInfoIndex;
use App\Models\WebInformation;
use Illuminate\Support\Facades\Route;



// Main route admin
Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified', 'locale', 'is_admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', [BaseController::class, 'dashboard'])->name('admin.dashboard');
        // Route::resource('/product', ProductController::class);
        // Route::resource('/article', ArticleController::class);
        // Route::resource('/category',CategoryController::class);
        Route::resource('/user', UserController::class);
        Route::resource('/channel', ChannelController::class);
        Route::resource('/partner', PartnerController::class);
        Route::resource('/order', OrderController::class);
        Route::resource('/infoweb', InfoWebController::class);
        Route::get('/info', [InformationController::class, 'show'])->name('info.show');
        Route::put('/info', [InformationController::class, 'save'])->name('info.save');
        Route::put('/update-password', [UserController::class, 'updatePassword'])->name('users.update-password');
        Route::get('change-language/{lang}', [BaseController::class, 'changeLanguage'])->name('admin.changeLanguage');
        Route::get('channel/{channelSlug}', [BaseController::class, 'channel'])->name('admin.channel');
        Route::get('posts/api', [PostController::class, 'getPosts']);

        //logo
        Route::get('web-info', WebInfoIndex::class)->name('webinfo.index');
        // Route::post('/',[LogoController::class,'changeLogoImage'])->name('change.logo');
        //File
        Route::get('file', FileIndex::class)->name('file.index');
        // Slide
        Route::get('slide',SlideIndex::class)->name('slide.index');
        Route::get('slide/create',SlideCreate::class)->name('slide.create');
        Route::get('slide/edit/{slide}',SlideEdit::class)->name('slide.edit');
        // Code 
        // Route::get('code',CodeIndex::class)->name('code.index');
        //Category 
        Route::get('category', CategoryIndex::class)->name('category.index');
        Route::get('category/create', CategoryCreate::class)->name('category.create');
        Route::get('category/edit/{category}', CategoryEdit::class)->name('category.edit');
        // Product
        Route::get('product', ProductIndex::class)->name('product.index');
        Route::get('product/create', ProductCreate::class)->name('product.create');
        Route::get('product/edit/{product}', ProductEdit::class)->name('product.edit');
        // Article
        Route::get('article', ArticleIndex::class)->name('article.index');
        Route::get('article/create', ArticleCreate::class)->name('article.create');
        Route::get('article/edit/{article}', ArticleEdit::class)->name('article.edit');
    });
});

//Jetstream livewire
Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
