<?php

use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\BaseController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\InformationController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ChannelController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PartnerController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileUserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.layouts.index');
// });

// Route::middleware(['locale'])->group(function(){
    Route::prefix('/')->group(function(){
        Route::get('',[HomeController::class, 'index'])->name('index');
        Route::get('detail/{slug}',[HomeController::class, 'productDetail'])->name('product.detail');
        Route::get('cate/{slug}',[HomeController::class, 'category'])->name('category.product.list');
        Route::get('article',[HomeController::class,'article']);
        Route::get('article/{slug}',[HomeController::class, 'articleDetail'])->name('article.detail');
        Route::get('contact',[HomeController::class, 'contact'])->name('contact');
        Route::get('storeContact',[HomeController::class,'storeContact'])->name('storeContact');
        Route::post('search',[HomeController::class, 'search'])->name('search');
        Route::get('profile',[ProfileUserController::class, 'index'])->name('profile.index');
        Route::put('profile/{id}',[ProfileUserController::class, 'update'])->name('profile.update');
    });
//});


