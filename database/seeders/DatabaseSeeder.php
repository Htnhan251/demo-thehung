<?php

namespace Database\Seeders;

use App\Models\Logo;
use App\Models\WebInformation;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        WebInformation::create( [
            'name' => 'logo',
            'value' => 'default',
        ]);

        WebInformation::create( [
            'name' => 'title',
            'value' => 'default',
        ]);
    }
}
