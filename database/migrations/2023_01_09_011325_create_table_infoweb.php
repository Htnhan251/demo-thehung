<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInfoweb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_webs', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->string('address_1');
            $table->string('address_2');
            $table->string('title_1');
            $table->text('colum_1');
            $table->string('title_2');
            $table->text('colum_2');
            $table->string('title_3');
            $table->text('colum_3');        
            $table->string('copyright');
            $table->boolean('is_active');           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_webs');
    }
}
