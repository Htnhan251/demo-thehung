<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableProductTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('locale',2); // en, vi
            $table->string('slug');
            $table->integer('product_id')->unsigned();
            $table->longText('long_description')->nullable();
            $table->text('short_description')->nullable();
            $table->text('image')->nullable();
            $table->text('title')->nullable();
            $table->timestamps();
            $table->unique(['product_id', 'locale']);
        });

        Schema::table('product_translations', function (Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');
    }
}
